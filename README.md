# Smartcampus Platform Smartbuilding Services

This service system is a smart building service for Smartcampus platform.


---

* [Language](#language)
* [List of Inventory Services](#list-of-inventory-services)
  - [Registering Stock](#registering-stock)
  - [Registering Temporary Stock](#registering-temporary-stock) 
  - [Getting Temoprary Stock](#getting-temporary-stock) 
  - [Deleting Temporary Stock](#deleting-tempopary-stock) 
  - [Processing Requirement Stock](#proseccing-requirement-stock) 
  - [Getting Requirement Stock](#getting-requirement-stock) 
  - [Getting Spesification Stock](#getting-specification-stock) 
  - [Registering Stock Description](#registering-specification-stock)  
  - [Getting All Stock](#getting-all-stock)
  - [Getting All Room](#getting-all-room) 
  - [Getting Stock Condition](#getting-stock-condition) 
* [List of Permission Services](#list-of-permission-services)
  - [Getting List Permission](#getting-list-permission)
  - [Getting List Building](#getting-list-building)
  - [Registering Permission](#registering-permission)
  - [Registering Invoice Permission](#registering-invoice-permission)
  - [Getting Invoice Permission](#getting-invoice-permission)
  - [Verification Permission](#verification-permission)
  - [Verification User Permission](#getting-user-permission)
  - [Registering Assign Staf](#registering-assign-staff)
  - [Getting Assign Staf](#getting-assign-staff)
  - [Registering Available Status](#registering-available-status)
  - [Cancel Permission](#cancel-permission) 
* [List of Stock Opname Services](#list-of-stock-opname-services)
  - [Updating Stock](#updating-stock)
* [List of Complaint Services](#list-of-complaint-services) 
  - [Getting Complaint](#getting-complaint)
  - [Registering Complaint](#registering-complaint)
  - [Cancel Complaint](#cancel-complaint)
* [List of Maintenance Services](#list-of-maintenance-services) 
  - [Registering Schedule](#registering-scedhule)
  - [Getting Schedule](#getting-scedhule) 
  - [Getting Unit](#getting-unit)
  - [Getting Trouble](#getting-trouble)
* [List of Analysis Services](#list-of-analysis-services) 
  - [Getting Recommendation](#getting-recommendation) 
* [Database](#database)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

---

## Language

This services are written in **PHP**

## List of Inventory Services


### **Registering Stock**

---

API for registering stock.

* **URL**

  ```
  /index.php/inventory/setKebutuhan
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], asset_id=[integer], jumlah=[integer], keterangan=[varchar], status_id=[integer]`

* **Success Response:**

   * **Status:** Created <br />
  * **Code:** 201 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/inventory/setKebutuhan",
    type: "POST",
    dataType: {"unit_id":"unit_id","asset_id":"asset_id","jumlah":"jumlah","keterangan":"keterangan","status_id":"status_id"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```

### **Registering Temporary Stock**

---

API for registering tempopary stock.

* **URL**

  ```
  /index.php/inventory/setTempKebutuhan
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `kebutuhan_id=[varchar], asset_id=[integer], jumlah=[integer], keterangan=[varchar]`

* **Success Response:**

   * **Status:** Created <br />
  * **Code:** 201 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/inventory/setTempKebutuhan",
    type: "POST",
    dataType: {"kebutuhan_id":"kebutuhan_id","asset_id":"asset_id","jumlah":"jumlah","keterangan":"keterangan"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```

### **Getting Temporary Stock**

---

API for getting temporary stok by id.

* **URL**

  ```
  /index.php/inventory/getTempKebutuhan?kid
  ```

* **Method:**

  `GET`

* **Success Response:**

  * 
    **Content:** `[{"id":"id","kebutuhan_id":"kebutuhan_id","asset_id":"asset_id","jumlah":"jumlah","activated":"activated","keterangan":"keterangan","barang":"barang","type":"type","desc":"desc"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getTempKebutuhan?kid=4d1b8ef2
  ```

### **Deleting Temporary Stock**

---

API for deleting temporary stok by id.

* **URL**

  ```
  /index.php/inventory/deleteTempKebutuhan?did
  ```

* **Method:**

  `DELETE`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `did=[varchar]`


* **Success Response:**

  * 
    * **true** 

* **Sample Call:**

  ```
  curl -i /index.php/inventory/deleteTempKebutuhan?did=5
  ```

  
### **Processing Requirement Stock**

---

API for processing requirement stock by unit_id.

* **URL**

  ```
  /index.php/inventory/prosesKebutuhan?unit_id
  ```

* **Method:**

  `GET`

* **Success Response:**

  * **true:** 

* **Sample Call:**

  ```
  curl -i /index.php/inventory/prosesKebutuhan?unit_id=5
  ```
  
### **Getting Requirement Stock**

---

API for getting requirement stock.

* **URL**

  ```
  /index.php/inventory/getKebutuhan
  ```

* **Method:**

  `GET`

* **Success Response:**

  * 
    **Content:** `[{"id":"id","kebutuhan_id":"kebutuhan_id","unit_id":"unit_id","asset_id":"asset_id","jumlah":"jumlah","keterangan":"keterangan","status_id":"status_id","unit_id_verifikasi": "unit_id_verifikasi","date":"date","name":"name","barang":"barang","type":"type","desc":"desc"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getKebutuhan
  ```
  
### **Getting Spesification Stock**

---

API for getting spesification stock.

* **URL**

  ```
  /index.php/inventory/getSpesification
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","asset_id":"asset_id","name":"name","type":"type","desc":"desc","rfid_code":"rfid_code"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getKebutuhan
  ```
  
### **Registering Stock Description**

---

API for registering stock description.

* **URL**

  ```
  /index.php/inventory/setAssetDescription
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `kebutuhan_id=[varchar], unit_id=[integer], asset_id=[integer], jumlah=[integer], keterangan=[varchar], status_id=[integer], unit_id_verifikasi=[integer]`

* **Success Response:**

   * **Status:** Created <br />
  * **Code:** 201 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/inventory/setAssetDescription",
    type: "POST",
    dataType: {"kebutuhan_id"="kebutuhan_id","unit_id"="unit_id","asset_id"="asset_id","jumlah","keterangan"="keterangan","status_id"="status_id","unit_id_verifikasi"="unit_id_verifikasi"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```
    
### **Getting All Stock**

---

API for getting all stock.

* **URL**

  ```
  /index.php/inventory/getAssetList
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","sn":"sn","unit_id":"unit_id","ruang_id":"ruang_id","asset_id":"asset_id","kondisi_id":"kondisi_id","date":"date","activated":"activated","name":"name","barang":"barang","type":"type","desc":"desc","ruang":"ruang","kondisi":"kondisi"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getAssetList
  ```

### **Getting All Room**

---

API for getting all room.

* **URL**

  ```
  /index.php/inventory/getRuangan
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","unit_id":"unit_id","ruang_id":"ruang_id","deskripsi":"deskripsi","rent":"rent","biaya":"biaya"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getRuangan
  ```
  
 ### **Getting Stock Condition**

---

API for getting stock condition.

* **URL**

  ```
  /index.php/inventory/getKondisi
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","kondisi_id":"kondisi_id","kondisi":"kondisi"}]`

* **Sample Call:**

  ```
  curl -i /index.php/inventory/getKondisi
  ``` 
  
## List of Permission Services

### **Getting List Permission**

---

API for getting list permission.

* **URL**

  ```
  /index.php/perizinan/getListApplication
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","unit_id":"unit_id","user_id":"user_id","ruang_id":"ruang_id","date_in":"date_in","date_out":"date_out","deskripsi":"deskripsi","status":"status","nama_depan":"nama_depan","nama_belakang":"nama_belakang","ruang":"ruang","biaya":"biaya"}]`

* **Sample Call:**

  ```
  curl -i /index.php/perizinan/getListApplication
  ``` 
 
### **Getting List Building**

---

API for getting list building.

* **URL**

  ```
  /index.php/perizinan/getGedung
  ```

* **Method:**

  `GET`

* **Success Response:**
* 
    **Content:** `[{"id":"id","unit_id":"unit_id","ruang_id":"ruang_id","deskripsi":"deskripsi","rent":"rent","biaya":"biaya"}]`

* **Sample Call:**

  ```
  curl -i /index.php/perizinan/getGedung
  ```

### **Registering Permission**

---

API for registering permission.

* **URL**

  ```
  /index.php/perizinan/setApplication
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], user_id=[varchar], ruang_id=[integer], date_in=[date], date_out=[date], deskripsi=[varchar]`

* **Success Response:**
  
  * **true**

* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/setApplication",
    type: "POST",
    dataType: {"unit_id"="unit_id", "user_id"="user_id", "ruang_id"="ruang_id", "date_in"="date_in", "date_out"="date_out", "deskripsi"="deskripsi"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```
  
### **Registering Invoice Permission**

---

API for registering invoice permission.

* **URL**

  ```
  /index.php/perizinan/setInvoice
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer], status=[integer], jenis=[varchar]`

* **Success Response:**
 
  * **Code:** 200 <br />
  * **Status:** ok <br />
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/setInvoice",
    type: "POST",
    dataType: {"id"="id", "status="status, "jenis"="jenis"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```
     
### **Getting Invoice Permission**

---

API for getting invoice permission.

* **URL**

  ```
  /index.php/perizinan/getInvoice?id
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer]`

* **Success Response:**
* 
    **Content:** `[{"id":"id","unit_id":"unit_id","user_id":"user_id","ruang_id":"ruang_id","date_in"="date_in", "date_out"="date_out","deskripsi":"deskripsi","status":"status","nama_depan":"nama_depan","nama_belakang":"nama_belakang","ruang":"ruang","biaya":"biaya"}]`

* **Sample Call:**

  ```
  curl -i /index.php/perizinan/getInvoice?id=1
  ```
   
### **Verification Permission**

---

API for verification permission.

* **URL**

  ```
  /index.php/perizinan/verification
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer], status=[integer]`

* **Success Response:**
 
  * **Code:** 200 <br />
  * **Status:** ok <br />
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/verification",
    type: "POST",
    dataType: {"id"="id", "status="status}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```
  
### **Verification User Permission**

---

API for verification user permission.

* **URL**

  ```
  /index.php/perizinan/doVerification
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer], status=[integer], oleh=[varchar]`

* **Success Response:**
 
  * **Code:** 200 <br />
  * **Status:** ok <br />
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/doVerification",
    type: "POST",
    dataType: {"id"="id", "status="status, "oleh"="user_id" }
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ```
 
### **Registering Assign Staf**

---

API for registering assign staf.

* **URL**

  ```
  /index.php/perizinan/setAssignStaff
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer], staff=[varchar]`

* **Success Response:**
 
  * **Code:** 200 <br />
  * **Status:** ok <br />
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/setAssignStaff",
    type: "POST",
    dataType: {"id"="id", "staff="staff}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 
  
### **Getting Assign Staf**

---

API for getting sssign staf.

* **URL**

  ```
  /index.php/perizinan/getAssignStaff?id
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer]`

* **Success Response:**
* 
    **Content:** ``

* **Sample Call:**

  ```
  curl -i /index.php/perizinan/getAssignStaff?id
  ```
 
### **Cancel Permission**

---

API for cancel permission.

* **URL**

  ```
  /index.php/perizinan/batal
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer]`

* **Success Response:**
 
  * **true:** 
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/batal",
    type: "POST",
    dataType: {"id"="id", "staff="staff}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 

## List of Stock Opname Services

### **Updating Stock**

---

API for updating stock.

* **URL**

  ```
  /index.php/inventory/updateKondisi
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], kondisi_id=[integer]`

* **Success Response:**

  * **Status:** updated <br />
  * **Code:** 202 <br />
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perizinan/batal",
    type: "POST",
    dataType: {"id"="id", "staff="staff}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 

## List of Complaint Services

### **Getting Complaint**

---

API for getting complaint.

* **URL**

  ```
  /index.php/keluhan/getTrouble?unit_id=&user_id
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], user_id=[varchar]`

* **Success Response:**
* 
    **Content:** ` "id":"id","unit_id":"unit_id","user_id":"user_id","judul":"judul","date":"date","deskripsi":"deskripsi","status":"status","solution":"solution","nama_depan":"nama_depan","nama_belakang":"nama_belakang","unit": "unit"`

* **Sample Call:**

  ```
  curl -i /index.php/keluhan/getTrouble?unit_id=002&user_id=40-8451155
  ```
  
### **Registering Complaint**

---

API for registering complaint.

* **URL**

  ```
  /index.php/keluhan/setTrouble
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], user_id=[varchar], date=[date], judul=[varchar], deskripsi=[varchar]`

* **Success Response:**
 
  * **true:**
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/keluhan/setTrouble",
    type: "POST",
    dataType: {"unit_id"="unit_id", "user_id"="user_id", "date"="date", "judul"="judul", "deskripsi"="deskripsi"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 

### **Cancel Complaint**

---

API for cancel complaint.

* **URL**

  ```
  /index.php/keluhan/batal
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `id=[integer]`

* **Success Response:**
 
  * **true:** 
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/keluhan/batal",
    type: "POST",
    dataType: {"id"="id", "staff="staff}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 
 
## List of Maintenance Services

### **Registering Schedule**

---

API for registering schedule.

* **URL**

  ```
  /index.php/perawatan/setSchedule
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `unit_id=[integer], user_id=[varchar], date=[date], judul=[varchar],unit_id=[integer],deskripsi=[varchar]`

* **Success Response:**
 
  * **true:**
  
* **Sample Call:**

  ```
  $.ajax({
    url: "/index.php/perawatan/setSchedule",
    type: "POST",
    dataType: {"unit_id"="unit_id", "user_id"="user_id", "date"="date", "judul"="judul","unit_id"="unit_id", "deskripsi"="deskripsi"}
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
    // function if success
    },
    error: function (err) {
    // function if error
    }
  });
  ``` 
 
### **Getting Schedule**

---

API for getting scedhule.

* **URL**

  ```
  /index.php/perawatan/getSchedule
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  

* **Success Response:**
* 
    **Content:** ` "id":"id", "schedule":"scedhule", "asset_condition":"asset_condition", "maintenance":"maintenance", "assign_staff":"assign_staff", "solution":"solution", "status":"status", "oleh":"oleh", "asset":"asset", "unit_id":"unit_id", "nama_depan":"nama_depan", "nama_belakang":"nama_depan", "unit":"unit"`

* **Sample Call:**

  ```
  curl -i /index.php/perawatan/getSchedule
  ```
  
 ### **Getting Unit**

---

API for getting unit.

* **URL**

  ```
  /index.php/perawatan/getUnit
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  

* **Success Response:**
* 
    **Content:** ` "id":"id", "unit_id":"unit_id", "name":"name", "activated":"activated", "pejabat":"pejabat"`

* **Sample Call:**

  ```
  curl -i /index.php/perawatan/getUnit
  ```
 
 ### **Getting Trouble**

---

API for getting trouble.

* **URL**

  ```
  /index.php/perawatan/getTrouble
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  

* **Success Response:**
* 
    **Content:** ` "barang":"barang", "type":"type", "jumlah":"jumlah", "recommendation":"recommendation"`

* **Sample Call:**

  ```
  curl -i /index.php/perawatan/getTrouble
  ```

## List of Analysis Services

### **Getting Recommendation**
  ---

API for getting recommendation.

* **URL**

  ```
  /index.php/analisa/listRekomendasi
  ```

* **Method:**

  `GET`
  
* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  

* **Success Response:**
* 
    **Content:**  

* **Sample Call:**

  ```
  curl -i /index.php/analisa/listRekomendasi
  ```

## Database

* postgre.sql


## Built With

* [CI](http://www.codeigniter.com/) - The web framework used

## Authors

* **Saiful Rizal (23218009)** - [Saiful Rizal](https://gitlab.com/izalious)
* **Arif Rachmat (23218089)** - [Arif Rachmat](https://gitlab.com/ernidianawati)
* **Erni Dianawati (23218055)** - [Erni Dianawati](https://gitlab.com/aiepbug)

## License

This project is licensed under the MIT License

## Acknowledgments

* Prof. Suhardi as lecturer for PLTI course
* Mr. Novianto and Mr. Wardani as assistant for PLTI course
