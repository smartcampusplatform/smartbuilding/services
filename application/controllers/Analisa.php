<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisa extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model("ModelInventory","m");
		$this->session->set_userdata(array("unit_id"=>"002"));

	}

	public function listRekomendasi()
	{
		$unit_id = $this->session->userdata("unit_id");
		$q = $this->db->select("ref_barang.*")->from("ref_barang")->get()->result();
		$result = array();
		foreach($q as $r)
		{
			$s = $this->db->select("trans_stock.*")->from("trans_stock")->where("trans_stock.asset_id",$r->asset_id)->get()->num_rows();
			if($r->min_pejabat ==1)
			{
				$p = $this->jumlahPejabat();
			}
			else
			{
				$p=0;
			}
			$data ["barang"] = $r->name;
			$data ["type"] = $r->type;
			$data ["jumlah"] = $s;
			if($p<$s){
				$data ["rekomendasi"] = 0;
			}
			else {
				$data ["rekomendasi"] = $p-$s;
			}
			$result[] = $data;
		}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function jumlahPejabat()
	{
		$unit_id = $this->session->userdata("unit_id");
		$q = $this->db->select("ref_unit.pejabat")->from("ref_unit")->where("ref_unit.unit_id",$unit_id)->get()->result();
		return $q[0]->pejabat;
	}
}
