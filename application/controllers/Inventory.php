<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model("ModelInventory","m");
		$this->session->set_userdata(array("unit_id"=>"002"));

	}

	public function index()
	{
		echo "service available : [setKebutuhan(),\n getKebutuhan(),\n getSpesification(),\n setAssetDescription()]";
	}
	public function setKebutuhan()
	{
		$unit_id = $this->session->userdata("unit_id");
		$asset_id = $this->input->post("asset_id");
		$jumlah = $this->input->post("jumlah");
		$keterangan = $this->input->post("keterangan");
		$status_id  = $this->input->post("status_id");
		if($unit_id=="" or $asset_id=="" or $jumlah=="" or $keterangan=="" or $status_id=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		else
		{
			$kebutuhan_id = uniqid("\r");
			$q = $this->m->setKebutuhan(trim($kebutuhan_id),$unit_id,$asset_id,$jumlah,$keterangan,$status_id);
			if($q==true)
			{
				$m = array("status"=>"created","code"=>"201");
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($m));
		}
	}
	public function setTempKebutuhan()
	{
		$kebutuhan_id = $this->input->post("kebutuhan_id");
		$asset_id = $this->input->post("asset_id");
		$jumlah = $this->input->post("jumlah");
		$keterangan = $this->input->post("keterangan");
		if($kebutuhan_id=="" or $asset_id=="" or $jumlah=="" or $keterangan=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		else
		{
			$q = $this->m->setTempKebutuhan(trim($kebutuhan_id),$asset_id,$jumlah,$keterangan);
			if($q==true)
			{
				$m = array("status"=>"created","code"=>"201");
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($m));
		}
	}
	public function getTempKebutuhan()
	{
		$kebutuhan_id = $this->input->get("kid");
		if($kebutuhan_id=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->m->getTempKebutuhan($kebutuhan_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function deleteTempKebutuhan()
	{
		$id = $this->input->post("did");
		$q = $this->m->deleteTempKebutuhan($id);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function prosesKebutuhan()
	{
		$unit_id = $this->session->userdata("unit_id");
		$id = $this->input->post("did");
		$q = $this->m->prosesKebutuhan($id,$unit_id);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getKebutuhan()
	{
		$kebutuhan_id = $this->input->get("kebutuhan_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->m->getKebutuhan($kebutuhan_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getSpesification()
	{
		$name = $this->input->get("name");
		$asset_id = $this->input->get("asset_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->m->getListBarang($asset_id,$limit,$offset,$name)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setAssetDescription()
	{
		$kebutuhan_id = $this->input->post("kebutuhan_id");
		$unit_id = $this->input->post("unit_id");
		$asset_id = $this->input->post("asset_id");
		$jumlah = $this->input->post("jumlah");
		$keterangan = $this->input->post("keterangan");
		$status_id = $this->input->post("status_id");
		$unit_id_verifikasi = $this->session->userdata("unit_id");
		if($kebutuhan_id=="" or $unit_id=="" or $asset_id=="" or $jumlah=="" or $status_id=="" or $keterangan=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		else
		{
			$q = $this->m->setAssetDescription($kebutuhan_id,$unit_id,$asset_id,$jumlah,$status_id,$unit_id_verifikasi,$keterangan);
			if($q==true)
			{
				$m = array("status"=>"created","code"=>"201");
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($m));
		}
	}
	public function getAssetList()
	{
		$unit_id = $this->session->userdata("unit_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$ruang = $this->input->get("ruang");
		$sn = $this->input->get("sn");
		$q = $this->m->getAssetList($unit_id,$limit,$offset,$ruang,$sn)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getRuangan()
	{
		$unit_id = $this->session->userdata("unit_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->m->getRuangan($unit_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getKondisi()
	{
		$unit_id = $this->session->userdata("unit_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->m->getKondisi($unit_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function updateKondisi()
	{
		$unit_id = $this->session->userdata("unit_id");
		$kondisi_id = $this->input->post("kondisi_id");
		$sn = $this->input->post("sn");
		$q = $this->m->updateKondisi($unit_id,$kondisi_id,$sn);
		if($q==true)
		{
			$m = array("status"=>"updated","code"=>"202");
			$this->output->set_content_type('application/json')->set_output(json_encode($m));
		}
	}
}
