<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluhan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model("ModelInventory","m");
		$this->load->model("ModelPerizinan","i");
		$this->load->model("ModelKeluhan","k");
		$this->session->set_userdata(array("unit_id"=>"002","user_id"=>"40-8441155"));

	}

	public function index()
	{
		echo "service available Inventory : [setKebutuhan(),\n getKebutuhan(),\n getSpesification(),\n setAssetDescription()]";
		echo "service available Perizinan : [setApplication(),\n getListApplication()]";
		echo "service available Keluhan : [setTrouble(),\n getTrouble()]";
	}
	public function getTrouble()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		if($user_id=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->k->getTrouble($unit_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setTrouble()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		$date = date("Y-m-d");
		$judul = $this->input->post("judul");
		$deskripsi = $this->input->post("deskripsi");
		$q = $this->k->setTrouble($unit_id,$user_id,$date,$judul,$deskripsi);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setSolution()
	{
		$solution = $this->input->post("solution");
		$id = $this->input->post("id");
		$q = $this->k->setSolution($id,$solution);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}

	}
	public function batal()
	{
    $id = $this->input->post("id");
		$q = $this->i->batal($id);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
}
