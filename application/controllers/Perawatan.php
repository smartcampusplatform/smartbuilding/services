<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawatan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model("ModelInventory","m");
		$this->load->model("ModelPerizinan","i");
		$this->load->model("ModelKeluhan","k");
		$this->load->model("ModelPerawatan","r");
		$this->session->set_userdata(array("unit_id"=>"008","user_id"=>"99-6132128"));

	}

	public function index()
	{
		echo "service available Inventory : [setKebutuhan(),\n getKebutuhan(),\n getSpesification(),\n setAssetDescription()]";
		echo "service available Perizinan : [setApplication(),\n getListApplication()]";
		echo "service available Keluhan : [setTrouble(),\n getTrouble()]";
		echo "service available Perawatan : [setSchedule(),\n getSchedule()]";
	}
	public function setSchedule()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		$schedule = $this->input->post("date");
		$condition = $this->input->post("judul");
		$unit_id = $this->input->post("unit_id");
		$maintenance = $this->input->post("deskripsi");
		$q =$this->r->setSchedule($schedule,$condition,$maintenance,$user_id,$unit_id);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getSchedule()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q =$this->r->getSchedule($unit_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getUnit()
	{
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q =$this->r->getUnit($limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getTrouble()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		if($user_id=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->k->getTrouble($unit_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}

}
