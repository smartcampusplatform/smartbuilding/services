<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perizinan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model("ModelInventory","m");
		$this->load->model("ModelPerizinan","i");
		$this->session->set_userdata(array("unit_id"=>"002","user_id"=>"40-8441155"));

	}

	public function index()
	{
		echo "service available Inventory : [setKebutuhan(),\n getKebutuhan(),\n getSpesification(),\n setAssetDescription()]";
		echo "service available Perizinan : [setKebutuhan(),\n getKebutuhan(),\n getSpesification(),\n setAssetDescription()]";
	}
	public function getListApplication()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		if($user_id=="")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"400",
				"status"=>"bad request",
				"notes"=>"missing data"
			)));
			return false;
		}
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$q = $this->i->getListApplication($user_id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function getGedung()
	{
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$id = $this->input->get("id");
		$q = $this->i->getGedung($id,$limit,$offset)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setApplication()
	{
		$unit_id = $this->session->userdata("unit_id");
		$user_id = $this->session->userdata("user_id");
		$ruang_id = $this->input->post("ruang_id");
		$date_in = $this->input->post("date_in");
		$date_out = $this->input->post("date_out");
		$deskripsi = $this->input->post("deskripsi");
		$q = $this->i->simpanIzin($unit_id,$user_id,$ruang_id,$date_in,$date_out,$deskripsi);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setInvoice()
	{
		$id = $this->input->post("id");
		$status = $this->input->post("status");
		$jenis = $this->input->post("jenis");
		$q = $this->i->setInvoice($jenis,$id,$status);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}
	}
	public function getInvoice()
	{
		$id = $this->input->get("id");
		$q = $this->i->getInvoice($id)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function verification()
	{
		$id = $this->input->post("id");
		$status = $this->input->post("status");
		$q = $this->i->verification($id,$status);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}
	}
	public function doVerification()
	{
		$id = $this->input->post("id");
		$status = $this->input->post("status");
		$oleh = $this->session->userdata("user_id");
		$q = $this->i->doVerification($id,$status,$oleh);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}
	}
	public function setAssignStaff()
	{
		$id = $this->input->post("id");
		$staff = $this->input->post("staff");
		$table = $this->input->post("table");
		$q = $this->i->setAssignStaff($id,$staff,$table);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}
	}
	public function getAssignStaff()
	{
		$id = $this->input->get("id");
		$table = $this->input->get("table");
		$q = $this->i->getAssignStaff($id,$table)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
	public function setAvailableStatus()
	{
		$id = $this->input->post("id");
		$status = $this->input->post("status");
		$q = $this->i->setAvailableStatus($id,$status,$oleh);
		if($q=="True")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				"code"=>"200",
				"status"=>"ok"
			)));
		}
	}
	public function getNotification()
	{
		$id = $this->input->get("id");
		$table = $this->input->get("table");
		$q = $this->i->getNotification($id,$table)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}

	public function batal()
	{
    $id = $this->input->post("id");
		$q = $this->i->batal($id);
		$this->output->set_content_type('application/json')->set_output(json_encode($q));
	}
}
