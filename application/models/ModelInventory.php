<?php
class ModelInventory extends CI_Model {
    public function setKebutuhan($kebutuhan_id,$unit_id,$asset_id,$jumlah,$keterangan,$status_id)
    {
      $data = array(
          "kebutuhan_id" => $kebutuhan_id,
          "unit_id" => $unit_id,
          "asset_id" => $asset_id,
          "jumlah" => $jumlah,
          "keterangan" => $keterangan,
          "status_id" => $status_id,
          "date" => date("Y-m-d h:m:s")
        );
      return $this->db->insert("trans_kebutuhan",$data);
    }
    public function setTempKebutuhan($kebutuhan_id,$asset_id,$jumlah,$keterangan)
    {
      $data = array(
          "kebutuhan_id" => $kebutuhan_id,
          "asset_id" => $asset_id,
          "jumlah" => $jumlah,
          "keterangan" => $keterangan
        );
      return $this->db->insert("temp_trans_kebutuhan",$data);
    }
    public function getTempKebutuhan($kebutuhan_id,$limit,$offset)
    {
      $this->db->select("
        temp_trans_kebutuhan.*,
        ref_barang.name AS barang,
        ref_barang.type,
        ref_barang.desc
        ")
        ->from("temp_trans_kebutuhan")
        ->join("ref_barang","temp_trans_kebutuhan.asset_id = ref_barang.asset_id","left")
        ->where(array("kebutuhan_id"=>$kebutuhan_id,"temp_trans_kebutuhan.activated"=>"1"))
        ->order_by("id","asc");
        return $this->db->limit($limit,$offset)->get();
    }
    public function deleteTempKebutuhan($id)
    {
      return $this->db->where("id",$id)->update("temp_trans_kebutuhan",array("activated"=>"0"));
    }
    public function prosesKebutuhan($id,$unit_id)
    {
      $d = $this->db->select("*")->from("temp_trans_kebutuhan")->where(array("kebutuhan_id"=>$id,"activated"=>"1"))->get()->result();
      foreach($d as $c)
      {
        $this->db->insert("trans_kebutuhan",array(
            "kebutuhan_id"=>$c->kebutuhan_id,
            "unit_id"=>$unit_id,
            "asset_id"=>$c->asset_id,
            "jumlah"=>$c->jumlah,
            "keterangan"=>$c->keterangan,
            "status_id"=>"1",
            "date"=>date("Y-m-d")
          )
        );
      }
      return $this->db->where("kebutuhan_id",$id)->update("temp_trans_kebutuhan",array("activated"=>"2"));
    }
    public function getKebutuhan($kebutuhan_id,$limit,$offset)
    {
      $this->db->select("trans_kebutuhan.*,ref_unit.name,
        ref_barang.name AS barang,
        ref_barang.type,
        ref_barang.desc
        ")
        ->from("trans_kebutuhan")
        ->join("ref_barang","trans_kebutuhan.asset_id = ref_barang.asset_id","left")
        ->join("ref_unit","trans_kebutuhan.unit_id = ref_unit.unit_id","left");
        if($kebutuhan_id!=null)
        {
          $this->db->where("kebutuhan_id",$kebutuhan_id);
        }
        return $this->db->limit($limit,$offset)->get();
    }
    public function getListBarang($asset_id,$limit,$offset,$name)
    {
      $this->db->select("ref_barang.id,ref_barang.asset_id,ref_barang.name,ref_barang.type,ref_barang.desc,ref_barang.rfid_code,")
        ->from("ref_barang");
        if($asset_id!=null OR $asset_id!="")
        {
          $this->db->where("ref_barang.asset_id",$asset_id);
        }
        if($name!=null OR $name!="")
        {
          $this->db->like("LOWER(name)", strtolower($name))->or_like("LOWER(type)", strtolower($name));
        }
        return $this->db->where("activated",1)->limit($limit,$offset)->get();
    }
    public function setAssetDescription($kebutuhan_id,$unit_id,$asset_id,$jumlah,$status_id,$unit_id_verifikasi,$keterangan)
    {
      $d = $this->db->select("*")->from("trans_kebutuhan")->where(
          array("kebutuhan_id"=>$kebutuhan_id,"unit_id"=>$unit_id,"asset_id"=>$asset_id)
        )->get();
      if($d->num_rows()!=1){
        return array("code"=>"404","status"=>"not found","notes"=>"record not found");
        return false;
      }
      else {
        $i = $this->db->insert("trans_kebutuhan",
          array(
            "kebutuhan_id"=>$kebutuhan_id,
            "unit_id"=>$unit_id,
            "asset_id"=>$asset_id,
            "keterangan"=>$keterangan,
            "jumlah"=>$jumlah,
            "unit_id_verifikasi"=>$unit_id_verifikasi,
            "date" => date("Y-m-d h:m:s")
          ));
        return $i;
      }

    }
    public function getAssetList($unit_id,$limit,$offset,$ruang,$sn)
    {
      $this->db->select("trans_stock.*,ref_unit.name,
        ref_barang.name AS barang,
        ref_barang.type,
        ref_barang.desc,
        ref_ruangan.deskripsi AS ruang,
        ref_kondisi.kondisi,
        ")
        ->from("trans_stock")
        ->join("ref_barang","trans_stock.asset_id = ref_barang.asset_id","left")
        ->join("ref_unit","trans_stock.unit_id = ref_unit.unit_id","left")
        ->join("ref_ruangan","trans_stock.ruang_id = ref_ruangan.ruang_id","left")
        ->join("ref_kondisi","trans_stock.kondisi_id = ref_kondisi.kondisi_id","left")
        ->where(array("trans_stock.unit_id"=>$unit_id,"trans_stock.activated"=>"1"));
      if($ruang!="" || $ruang!=null)
      {
        $this->db->where("trans_stock.ruang_id",$ruang);
      }
      if($sn!="" || $sn!=null)
      {
        $this->db->where("trans_stock.sn",$sn);
      }
      return $this->db->limit($limit,$offset)->get();
    }
    public function getRuangan($unit_id,$limit,$offset)
    {
      return $this->db->select("*")->from("ref_ruangan")
        ->where("unit_id",$unit_id)
        ->limit($limit,$offset)
        ->get();
    }
    public function getKondisi($unit_id,$limit,$offset)
    {
      return $this->db->select("*")->from("ref_kondisi")
        ->limit($limit,$offset)
        ->get();
    }
    public function updateKondisi($unit_id,$kondisi_id,$sn)
    {
      $data = array("unit_id"=>$unit_id,"sn"=>$sn);
      return $this->db->where($data)->update("trans_stock",array("kondisi_id"=>$kondisi_id));
    }
  }
