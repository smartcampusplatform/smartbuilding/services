<?php
class ModelPerawatan extends CI_Model {

    public function setSchedule($schedule,$condition,$maintenance,$user_id,$unit_id)
    {
      $data = array(
        "schedule"=>$schedule,
        "asset_condition"=>$condition,
        "maintenance"=>$maintenance,
        "oleh"=>$user_id,
        "unit_id"=>$unit_id,
        "status"=>"0"
      );
      return $this->db->insert("trans_perawatan",$data);
    }
    public function getUnit($limit,$offset)
    {
      return $this->db->select("*")->from("ref_unit")->get();
    }
    public function getSchedule($unit_id,$limit,$offset)
    {
      $this->db->select("
      trans_perawatan.*,
      ref_pegawai.nama_depan,
      ref_pegawai.nama_belakang,
      ref_unit.name as unit,
      ")
      ->from("trans_perawatan")
      ->join("ref_pegawai","trans_perawatan.oleh = ref_pegawai.pegawai_id","left")
      ->join("ref_unit","trans_perawatan.unit_id = ref_unit.unit_id","left");
      // ->where("ref_unit.unit_id",$unit_id);
      return $this->db->limit($limit,$offset)->get();
    }
    public function getTrouble($unit_id,$limit,$offset)
    {
      $this->db->select("
        trans_keluhan.*,
        ref_pegawai.nama_depan,
        ref_pegawai.nama_belakang,
        ref_unit.name as unit
        ")
        ->from("trans_keluhan")
        ->join("ref_pegawai","trans_keluhan.user_id = ref_pegawai.pegawai_id","left")
        ->join("ref_unit","trans_keluhan.unit_id = ref_unit.unit_id","left")
        ->where("trans_keluhan.unit_id",$unit_id)
        ->where("trans_keluhan.status !=","5");
        return $this->db->limit($limit,$offset)->get();
    }
    public function getGedung($id="0",$limit,$offset)
    {
      if($id!=0)
      {
        $this->db->where("ref_ruangan.ruang_id",$id);
      }
      return $this->db->select("*")->from("ref_ruangan")
        ->where("rent","1")
        ->limit($limit,$offset)
        ->get();
    }
    public function setTrouble($unit_id,$user_id,$date,$judul,$deskripsi)
    {
      $data = array(
        "unit_id" => $unit_id,
        "user_id" => $user_id,
        "date" => $date,
        "judul" => $judul,
        "deskripsi" => $deskripsi,
        "status" => "0",
      );
      return $this->db->insert("trans_keluhan",$data);
    }
    public function setSolution($id,$solution)
    {
      return $this->db->where("id",$id)->update("trans_keluhan",array("solution"=>$solution));
    }
    public function batal($id)
    {
      return $this->db->where("id",$id)->update("trans_perizinan",array("status"=>"5"));
    }
  }
