<?php
class ModelPerizinan extends CI_Model {

    public function getListApplication($user_id,$limit,$offset)
    {
      $this->db->select("
        trans_perizinan.*,
        ref_pegawai.nama_depan,
        ref_pegawai.nama_belakang,
        ref_ruangan.deskripsi as ruang,
        ref_ruangan.biaya
        ")
        ->from("trans_perizinan")
        ->join("ref_pegawai","trans_perizinan.user_id = ref_pegawai.pegawai_id","left")
        ->join("ref_ruangan","trans_perizinan.ruang_id = ref_ruangan.ruang_id","left")
        ->where("trans_perizinan.user_id",$user_id)
        ->where("trans_perizinan.status !=","5");
        return $this->db->limit($limit,$offset)->get();
    }
    public function getGedung($id="0",$limit,$offset)
    {
      if($id!=0)
      {
        $this->db->where("ref_ruangan.ruang_id",$id);
      }
      return $this->db->select("*")->from("ref_ruangan")
        ->where("rent","1")
        ->limit($limit,$offset)
        ->get();
    }
    public function simpanIzin($unit_id,$user_id,$ruang_id,$date_in,$date_out,$deskripsi)
    {
      $data = array(
        "unit_id" => $unit_id,
        "user_id" => $user_id,
        "ruang_id" => $ruang_id,
        "date_in" => $date_in,
        "date_out" => $date_out,
        "deskripsi" => $deskripsi,
        "status" => "0",
      );
      return $this->db->insert("trans_perizinan",$data);
    }
    public function batal($id)
    {
      return $this->db->where("id",$id)->update("trans_perizinan",array("status"=>"5"));
    }
    public function setInvoice($jenis,$id,$status)
    {
      return $this->db->where("id",$id)->update($jenis,array("status"=>$status));
    }
    public function getInvoice($id)
    {
      $this->db->select("
        trans_perizinan.*,
        ref_pegawai.nama_depan,
        ref_pegawai.nama_belakang,
        ref_ruangan.deskripsi as ruang,
        ref_ruangan.biaya
        ")
        ->from("trans_perizinan")
        ->join("ref_pegawai","trans_perizinan.user_id = ref_pegawai.pegawai_id","left")
        ->join("ref_ruangan", "trans_perizinan.ruang_id = ref_ruangan.ruang_id","left")
        ->where("trans_perizinan.id",$id)
        ->where("trans_perizinan.status !=","5");
        return $this->db->get();
    }
    public function verification($id,$status)
    {
      return $this->db->where("id",$id)->update("trans_perizinan",array("status"=>$status));
    }
    public function doVerification($id,$status,$oleh)
    {
      return $this->db->where("id",$id)->update("trans_perizinan",array(
        "status"=>$status,
        "oleh"=>$oleh,
        "date"=>date("Y-m-d")
      ));
    }
    public function setAvailableStatus($id,$status)
    {
      return $this->db->where("id",$id)->update("trans_perizinan",array("status"=>$status));
    }
    public function setAssignStaff($id,$staff,$table)
    {
      return $this->db->where("id",$id)->update($table,array("assign_staff"=>$staff));
    }
    public function getAssignStaff($id,$table)
    {
      $this->db->select($table.".*,
        ref_pegawai.nama_depan,
        ref_pegawai.nama_belakang
        ")
        ->from($table)
        ->join("ref_pegawai",$table.".assign_staff = ref_pegawai.pegawai_id","left")
        ->where($table.".id",$id);
        return $this->db->get();
    }
    public function getNotification($id,$table)
    {
      $this->db->select($table.".*,
        ref_pegawai.nama_depan,
        ref_pegawai.nama_belakang,
        ref_status_ajuan.deskripsi as status_deksripsi
        ")
        ->from($table)
        ->join("ref_pegawai",$table.".assign_staff = ref_pegawai.pegawai_id","left")
        ->join("ref_status_ajuan",$table.".status = ref_status_ajuan.code","left")
        ->where($table.".id",$id);
        return $this->db->get();
    }
  }
