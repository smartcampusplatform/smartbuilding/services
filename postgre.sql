/*
 Navicat Premium Data Transfer

 Source Server         : localhost-postgre
 Source Server Type    : PostgreSQL
 Source Server Version : 100007
 Source Host           : localhost:5432
 Source Catalog        : smartbuilding
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100007
 File Encoding         : 65001

 Date: 17/05/2019 05:39:16
*/


-- ----------------------------
-- Sequence structure for id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq";
CREATE SEQUENCE "public"."id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq1";
CREATE SEQUENCE "public"."id_seq1" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq2
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq2";
CREATE SEQUENCE "public"."id_seq2" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq3
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq3";
CREATE SEQUENCE "public"."id_seq3" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq4
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq4";
CREATE SEQUENCE "public"."id_seq4" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq5
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq5";
CREATE SEQUENCE "public"."id_seq5" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq6
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq6";
CREATE SEQUENCE "public"."id_seq6" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq7
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq7";
CREATE SEQUENCE "public"."id_seq7" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for id_seq8
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."id_seq8";
CREATE SEQUENCE "public"."id_seq8" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sequence_keluhan
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sequence_keluhan";
CREATE SEQUENCE "public"."sequence_keluhan" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sequence_perawatan
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sequence_perawatan";
CREATE SEQUENCE "public"."sequence_perawatan" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sequence_perizinan
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sequence_perizinan";
CREATE SEQUENCE "public"."sequence_perizinan" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for trans_pengadaan_id
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."trans_pengadaan_id";
CREATE SEQUENCE "public"."trans_pengadaan_id" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_plus1
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_id_plus1";
CREATE SEQUENCE "public"."user_id_plus1" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for ref_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_barang";
CREATE TABLE "public"."ref_barang" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq3'::regclass),
  "asset_id" varchar(20) COLLATE "pg_catalog"."default",
  "name" varchar(100) COLLATE "pg_catalog"."default",
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "desc" varchar(255) COLLATE "pg_catalog"."default",
  "rfid_code" varchar(200) COLLATE "pg_catalog"."default",
  "activated" int4,
  "min_pejabat" int2 DEFAULT 1,
  "rasio_min" varchar(10) COLLATE "pg_catalog"."default" DEFAULT 0,
  "rasio_max" varchar(10) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_barang
-- ----------------------------
INSERT INTO "public"."ref_barang" VALUES (9, '12353', 'Monitor', 'Fujitsu', 'M7DLA', 'TYCG1090034H0206A', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (5, '12349', 'Laptop', 'Fujitsu', 'Lifebook T580', 'Q0Y02022', 1, 1, '0', '0.35');
INSERT INTO "public"."ref_barang" VALUES (11, '12355', 'Notebook', 'Fijitsu', 'SH782', 'R3301567', 1, 1, '0', '0.35');
INSERT INTO "public"."ref_barang" VALUES (13, '12357', 'PRINTER', 'FUJI XEROX', 'Docuprint C3055DX', '101199 00373DA11C', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (14, '12358', 'PRINTER', 'HP', 'Laser Jet Pro M201dw', 'VNC3301831', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (16, '12360', 'Scanner', 'Fujitsu', 'fi 5530C2', '6817', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (19, '12363', 'UPS', 'APC', 'BX1100LI-MS', '3B1447X10243', 1, 1, '0', '0.5');
INSERT INTO "public"."ref_barang" VALUES (20, '12364', 'UPS', 'APC', 'BX1100LI-MS', '3B1502X21250', 1, 1, '0', '0.5');
INSERT INTO "public"."ref_barang" VALUES (1, '12345', 'PC Desktop', 'Lenovo', 'Think Centre M80', 'R87BEGW', 1, 1, '0', '0.5');
INSERT INTO "public"."ref_barang" VALUES (21, '12365', 'HAKI', 'Kemenkumham', 'Paten 00001', '112', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (15, '12359', 'PROYEKTOR', 'HITACHI', 'CP-EX300', 'F5AH11374', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (12, '12356', 'PC WORKSTATION', 'Dell', 'DELL RECISION RACK 7910', 'DL3T252', 1, 1, '0', '0.5');
INSERT INTO "public"."ref_barang" VALUES (10, '12354', 'Monitor', 'Dell', 'P2214Hb', 'CN0CYBHD742615494175', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (8, '12352', 'Monitor', 'HP', '2055H249', 'CN629QK1Y', 1, 1, '0', '0.3');
INSERT INTO "public"."ref_barang" VALUES (6, '12350', 'Mesin Absensi/Handkey', 'HIT CORPORATION', 'MAGIC G6800', '0ID6040036021900052', 1, 0, '3', '0.2');
INSERT INTO "public"."ref_barang" VALUES (18, '12362', 'Switch', 'Dell', 'Networking X1018', 'BSVRX42', 1, 0, '2', '0.3');
INSERT INTO "public"."ref_barang" VALUES (17, '12361', 'Server', 'Fujitsu', 'Primergy RX300S6', 'Y16T013913', 1, 0, '2', '0.3');
INSERT INTO "public"."ref_barang" VALUES (2, '12346', 'Firewall', 'Dell', 'Sonic Wall TX 300 w', '18B1691FC480', 1, 0, '2', '0.3');
INSERT INTO "public"."ref_barang" VALUES (3, '12347', 'GPS', 'GARMIN', 'Monterra', '2TG024200', 1, 0, '2', '0.3');
INSERT INTO "public"."ref_barang" VALUES (4, '12348', 'Harddisk External', 'Buffalo', 'Buffalo Mini Station', '75571105202865', 1, 1, '0', '0.5');
INSERT INTO "public"."ref_barang" VALUES (7, '12351', 'Mesin FAX', 'PANASONIC', 'KX-MB1520CX', 'ILAFA001636', 1, 1, '0', '0.1');

-- ----------------------------
-- Table structure for ref_kondisi
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_kondisi";
CREATE TABLE "public"."ref_kondisi" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq4'::regclass),
  "kondisi_id" int2,
  "kondisi" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_kondisi
-- ----------------------------
INSERT INTO "public"."ref_kondisi" VALUES (1, 1, 'Baik');
INSERT INTO "public"."ref_kondisi" VALUES (2, 2, 'Rusak Ringan');
INSERT INTO "public"."ref_kondisi" VALUES (3, 3, 'Rusak Berat');

-- ----------------------------
-- Table structure for ref_pegawai
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_pegawai";
CREATE TABLE "public"."ref_pegawai" (
  "id" int4 NOT NULL DEFAULT nextval('user_id_plus1'::regclass),
  "pegawai_id" varchar(20) COLLATE "pg_catalog"."default",
  "unit_id" varchar(5) COLLATE "pg_catalog"."default",
  "nama_depan" varchar(50) COLLATE "pg_catalog"."default",
  "nama_belakang" varchar(50) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "gender" varchar(10) COLLATE "pg_catalog"."default",
  "tempat_lahir" varchar(100) COLLATE "pg_catalog"."default",
  "tanggal_lahir" date,
  "status" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_pegawai
-- ----------------------------
INSERT INTO "public"."ref_pegawai" VALUES (2, '99-6132128', '008', 'Giuseppe', 'Rosier', 'grosier0@msu.edu', 'Male', 'Al Abyār', '1986-01-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (3, '75-3659793', '005', 'Marius', 'Tomasutti', 'mtomasutti1@skyrock.com', 'Male', 'Banjeru', '1974-07-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (4, '40-8441155', '005', 'Cathy', 'Deare', 'cdeare2@ifeng.com', 'Female', 'Kongsvinger', '1989-05-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (5, '81-9647419', '006', 'Carmon', 'Dyment', 'cdyment3@statcounter.com', 'Female', 'Kafue', '1982-12-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (6, '17-0528238', '005', 'Danya', 'Robillart', 'drobillart4@ifeng.com', 'Female', 'Maipú', '1993-12-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (7, '50-1910354', '005', 'Corri', 'Shakshaft', 'cshakshaft5@whitehouse.gov', 'Female', 'Barrio San Luis', '1993-01-22', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (8, '04-7586997', '003', 'Lonni', 'Giacobelli', 'lgiacobelli6@opera.com', 'Female', 'Borzechów', '1971-10-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (9, '50-5436926', '003', 'Vannie', 'Kelberer', 'vkelberer7@nyu.edu', 'Female', 'Macarse', '1996-08-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (10, '60-4115370', '002', 'Rasla', 'Jilkes', 'rjilkes8@engadget.com', 'Female', 'Polanica-Zdrój', '1973-06-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (11, '74-4624694', '007', 'Gwenni', 'Crosston', 'gcrosston9@sbwire.com', 'Female', 'Almere Stad', '1989-10-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (12, '25-6511054', '001', 'Quincey', 'Boecke', 'qboeckea@washington.edu', 'Male', 'Awgu', '1997-07-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (13, '11-3225816', '001', 'Sofia', 'Kerswell', 'skerswellb@wsj.com', 'Female', 'Tongjin', '1986-05-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (14, '58-8145670', '002', 'Veda', 'Straker', 'vstrakerc@delicious.com', 'Female', 'Lianyi', '1985-12-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (15, '62-5828061', '006', 'Madelle', 'Heintze', 'mheintzed@aol.com', 'Female', 'Pnikut', '1991-02-19', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (16, '60-7778388', '004', 'Dorella', 'Crayk', 'dcrayke@unicef.org', 'Female', 'Old City', '1995-04-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (17, '07-9513502', '008', 'Rutledge', 'Gartenfeld', 'rgartenfeldf@netlog.com', 'Male', 'Aoufous', '1977-02-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (18, '25-0863100', '001', 'Vikki', 'Kingsmill', 'vkingsmillg@amazonaws.com', 'Female', 'Indaial', '1976-10-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (19, '59-2620467', '001', 'Marc', 'Brumbie', 'mbrumbieh@webnode.com', 'Male', 'San Miguel', '1980-12-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (20, '75-2492808', '003', 'Chad', 'Coram', 'ccorami@stanford.edu', 'Female', 'Lashkar Gāh', '1983-09-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (21, '31-9978343', '003', 'Casie', 'Walker', 'cwalkerj@pcworld.com', 'Female', 'Duanjia', '1991-03-15', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (22, '95-8285411', '002', 'Brett', 'Riseborough', 'briseboroughk@nba.com', 'Male', 'Puncakmanis', '1978-06-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (23, '52-9973390', '003', 'Crissie', 'Brockbank', 'cbrockbankl@booking.com', 'Female', 'Haapajärvi', '1972-09-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (24, '41-5446202', '002', 'Feliza', 'Arnholdt', 'farnholdtm@flickr.com', 'Female', 'Orange Farm', '1986-11-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (25, '97-5892043', '006', 'Emelda', 'Coogan', 'ecoogann@bing.com', 'Female', 'Charlottetown', '1975-09-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (26, '04-4079630', '007', 'Ellwood', 'Borer', 'eborero@freewebs.com', 'Male', 'Viana', '1987-02-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (27, '77-9433134', '006', 'Shaw', 'Yerrell', 'syerrellp@theguardian.com', 'Male', 'Tyresö', '1986-06-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (28, '17-0047529', '006', 'Tyrus', 'Ullyatt', 'tullyattq@furl.net', 'Male', 'Ayotupas', '1973-06-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (29, '24-3267434', '005', 'Syman', 'Longford', 'slongfordr@nps.gov', 'Male', 'Tuburan', '1996-05-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (30, '35-6533874', '007', 'Lemmie', 'Siveyer', 'lsiveyers@newsvine.com', 'Male', 'Markaz Mudhaykirah', '1991-12-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (31, '72-8181256', '005', 'Freddie', 'Colthurst', 'fcolthurstt@sakura.ne.jp', 'Male', 'Santiago Puringla', '1987-09-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (32, '45-8952145', '003', 'Marijo', 'Bogaert', 'mbogaertu@elegantthemes.com', 'Female', 'Yasenskaya', '1981-12-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (33, '60-4737613', '002', 'Aimee', 'Addie', 'aaddiev@lulu.com', 'Female', 'Grenoble', '1993-07-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (34, '29-3852477', '005', 'Valeria', 'Carley', 'vcarleyw@spiegel.de', 'Female', 'Najin', '1986-06-12', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (35, '11-7510783', '003', 'Hunter', 'Bilt', 'hbiltx@t-online.de', 'Male', 'Dabao', '1979-08-19', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (36, '54-6706465', '002', 'Hildegarde', 'Beecker', 'hbeeckery@jimdo.com', 'Female', 'São Bento', '1990-07-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (37, '91-7184946', '005', 'King', 'Braden', 'kbradenz@cmu.edu', 'Male', 'Beausejour', '1994-01-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (38, '99-3233226', '006', 'Giraud', 'Fernehough', 'gfernehough10@senate.gov', 'Male', 'Santa Rosa de Cabal', '1993-01-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (39, '88-3617828', '006', 'Banky', 'Grimstead', 'bgrimstead11@jimdo.com', 'Male', 'Bacheng', '1974-02-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (40, '46-6559423', '003', 'Debra', 'Fairbrother', 'dfairbrother12@php.net', 'Female', 'Dayong', '1974-01-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (41, '46-6916003', '002', 'Coralie', 'Edmons', 'cedmons13@smugmug.com', 'Female', 'Nidek', '1997-10-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (42, '59-9948571', '006', 'Shannon', 'Sains', 'ssains14@dell.com', 'Female', 'Kolobovo', '1981-02-19', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (43, '66-3865831', '007', 'Powell', 'Carek', 'pcarek15@delicious.com', 'Male', 'Caronoan West', '1982-10-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (44, '42-3121062', '001', 'Cate', 'Bourley', 'cbourley16@state.tx.us', 'Female', 'Dahu', '1983-11-16', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (45, '75-2954546', '001', 'Luella', 'Mecozzi', 'lmecozzi17@wunderground.com', 'Female', 'Lwengo', '1973-11-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (46, '81-5050231', '002', 'Rochester', 'D''Ambrosi', 'rdambrosi18@so-net.ne.jp', 'Male', 'Schifflange', '1984-10-15', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (47, '32-9532333', '008', 'Brigham', 'Danilov', 'bdanilov19@amazon.com', 'Male', 'Antanifotsy', '1974-04-06', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (48, '84-7629361', '007', 'Boris', 'Plewes', 'bplewes1a@abc.net.au', 'Male', 'Béthune', '1984-06-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (49, '40-1595850', '002', 'Rusty', 'Ethington', 'rethington1b@dot.gov', 'Male', 'Łętownia', '1982-12-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (50, '04-9903688', '007', 'Jan', 'Vickery', 'jvickery1c@1und1.de', 'Male', 'Jaciara', '1978-02-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (51, '19-5034842', '004', 'Sybil', 'Bewsy', 'sbewsy1d@yellowpages.com', 'Female', 'Xiangqiao', '1992-10-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (52, '36-6337941', '002', 'Clayton', 'Simeoli', 'csimeoli1e@desdev.cn', 'Male', 'Tawaran', '1984-03-06', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (53, '13-2618710', '001', 'Britt', 'Creffeild', 'bcreffeild1f@loc.gov', 'Female', 'Kafr Şūr', '1975-08-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (54, '70-3152174', '002', 'Jordana', 'Horche', 'jhorche1g@huffingtonpost.com', 'Female', 'Pawitan', '1984-05-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (55, '40-3459310', '005', 'Carilyn', 'Betke', 'cbetke1h@usatoday.com', 'Female', 'Liangshui', '1987-08-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (56, '40-4346526', '002', 'Piper', 'Dwire', 'pdwire1i@columbia.edu', 'Female', 'Sainte-Luce-sur-Loire', '1990-11-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (57, '56-7083116', '004', 'Caldwell', 'Eidelman', 'ceidelman1j@mlb.com', 'Male', 'Opočno', '1988-07-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (58, '91-6893243', '003', 'Cletus', 'Bezant', 'cbezant1k@comcast.net', 'Male', 'Niederwaldkirchen', '1996-07-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (59, '74-8242345', '001', 'Lotty', 'Faustian', 'lfaustian1l@typepad.com', 'Female', 'Kefar Yona', '1978-10-16', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (60, '78-8113821', '005', 'Alfi', 'Escoffier', 'aescoffier1m@ehow.com', 'Female', 'Uyen Hung', '1974-02-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (61, '30-5963934', '002', 'Meagan', 'Champneys', 'mchampneys1n@trellian.com', 'Female', 'Taoyao', '1998-02-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (62, '02-1834806', '003', 'Fransisco', 'Whaites', 'fwhaites1o@xinhuanet.com', 'Male', 'Mikhaylovka', '1987-07-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (63, '66-3334970', '008', 'Rosalind', 'Hartwright', 'rhartwright1p@va.gov', 'Female', 'Bumba', '1993-12-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (64, '69-1923820', '007', 'Darcy', 'Satteford', 'dsatteford1q@squidoo.com', 'Male', 'Sukamanah', '1988-10-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (65, '71-5599578', '004', 'Kimmie', 'Tampin', 'ktampin1r@google.it', 'Female', 'Nunmanu', '1988-10-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (66, '47-1252262', '006', 'Antons', 'Treweke', 'atreweke1s@alibaba.com', 'Male', 'Bei’an', '1980-02-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (67, '13-0155104', '007', 'Aryn', 'Colnett', 'acolnett1t@jalbum.net', 'Female', 'Mari', '1980-01-04', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (68, '66-2152867', '006', 'Bald', 'Hudel', 'bhudel1u@hhs.gov', 'Male', 'Clonskeagh', '1981-05-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (69, '45-2975545', '001', 'Caroline', 'Pashba', 'cpashba1v@imgur.com', 'Female', 'Łanięta', '1983-01-21', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (70, '51-6728729', '002', 'Karlis', 'Battram', 'kbattram1w@wp.com', 'Male', 'Springfield', '1996-08-11', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (71, '13-7861832', '004', 'Dolf', 'Band', 'dband1x@cam.ac.uk', 'Male', 'Bardo', '1975-10-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (72, '90-9193417', '006', 'Leta', 'Dofty', 'ldofty1y@printfriendly.com', 'Female', 'Shazhouba', '1985-12-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (73, '98-5231098', '006', 'Bonnie', 'Fleckness', 'bfleckness1z@uiuc.edu', 'Female', 'Baliang', '1981-08-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (74, '07-3471618', '005', 'Giacopo', 'O''Dogherty', 'godogherty20@oaic.gov.au', 'Male', 'Ma‘bar', '1997-04-24', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (75, '08-3044454', '006', 'Beitris', 'Gooders', 'bgooders21@seesaa.net', 'Female', 'Mazamet', '1978-05-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (76, '18-8534855', '008', 'Sergei', 'Groven', 'sgroven22@cyberchimps.com', 'Male', 'Thành Phố Hạ Long', '1984-10-19', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (77, '41-3699089', '005', 'Alfi', 'Snow', 'asnow23@mlb.com', 'Female', 'Kutapanjang', '1982-02-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (78, '51-6469982', '001', 'Carlee', 'Iannuzzelli', 'ciannuzzelli24@prlog.org', 'Female', 'Krasnogorskiy', '1989-08-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (79, '16-0409066', '004', 'Devin', 'Dowrey', 'ddowrey25@prlog.org', 'Male', 'Pontal', '1990-12-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (80, '29-9160233', '005', 'Vivian', 'Beernaert', 'vbeernaert26@tumblr.com', 'Female', 'Jacksonville', '1990-02-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (81, '92-7704752', '004', 'Gordie', 'Leavesley', 'gleavesley27@rediff.com', 'Male', 'Moulay Yacoub', '1976-03-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (82, '13-1164827', '002', 'Melisse', 'Abramson', 'mabramson28@cbsnews.com', 'Female', 'Luohuang', '1986-04-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (83, '25-3101423', '008', 'Dinnie', 'Bierton', 'dbierton29@cloudflare.com', 'Female', 'Quipot', '1972-11-04', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (84, '99-1008244', '004', 'Gisele', 'Walden', 'gwalden2a@alibaba.com', 'Female', 'Ilare', '1990-08-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (85, '75-6545726', '007', 'Winifred', 'Pendlebery', 'wpendlebery2b@microsoft.com', 'Female', 'Weetobula', '1982-11-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (86, '34-8167040', '005', 'Nita', 'Matteini', 'nmatteini2c@who.int', 'Female', 'Shabqadar', '1994-06-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (87, '25-1946236', '005', 'Nevin', 'Nannizzi', 'nnannizzi2d@digg.com', 'Male', 'Twardogóra', '1996-11-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (88, '74-0581389', '008', 'Ruthe', 'Winterbottom', 'rwinterbottom2e@apache.org', 'Female', 'Ganhe', '1975-03-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (89, '29-7590118', '002', 'Josi', 'Succamore', 'jsuccamore2f@globo.com', 'Female', 'Klakeh', '1985-12-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (90, '06-4088834', '002', 'Vonni', 'Belward', 'vbelward2g@wisc.edu', 'Female', 'Shijia', '1978-11-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (91, '83-4914913', '007', 'Vassili', 'Jencken', 'vjencken2h@ocn.ne.jp', 'Male', 'Isehara', '1993-12-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (92, '35-0987972', '003', 'Klarrisa', 'Tabourier', 'ktabourier2i@ebay.co.uk', 'Female', 'San Vicente', '1995-12-20', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (93, '73-2004568', '003', 'Herbert', 'Weedon', 'hweedon2j@ocn.ne.jp', 'Male', 'Magog', '1979-08-31', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (94, '42-4118731', '004', 'Bay', 'Ciementini', 'bciementini2k@webeden.co.uk', 'Male', 'Sredno Konjare', '1990-03-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (95, '32-7793178', '008', 'Konstantin', 'Stannus', 'kstannus2l@hc360.com', 'Male', 'Sila Lat', '1997-05-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (96, '79-6215935', '003', 'Rosamund', 'Aronovich', 'raronovich2m@soup.io', 'Female', 'Buynaksk', '1990-09-12', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (97, '94-4323125', '004', 'Alanson', 'Huxton', 'ahuxton2n@topsy.com', 'Male', 'Moba', '1989-05-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (98, '56-1256647', '008', 'Parsifal', 'Morrowe', 'pmorrowe2o@about.me', 'Male', 'Vidovci', '1982-07-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (99, '57-4605577', '008', 'Ogden', 'Ghidoli', 'oghidoli2p@pbs.org', 'Male', 'Sidenreng', '1996-03-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (100, '97-5233036', '002', 'Padgett', 'Blumire', 'pblumire2q@discuz.net', 'Male', 'Gobernador Ingeniero Valentín Virasoro', '1981-07-05', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (101, '28-1163778', '007', 'Cristine', 'Scala', 'cscala2r@google.com.au', 'Female', 'Gataivai', '1987-03-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (102, '68-5052399', '002', 'Hyacinthe', 'Cuseick', 'hcuseick2s@rambler.ru', 'Female', 'Bang Racham', '1986-11-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (103, '51-3404727', '006', 'Phillipp', 'Boxill', 'pboxill2t@usnews.com', 'Male', 'Tabu', '1993-03-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (104, '51-8632456', '004', 'Meris', 'Trunks', 'mtrunks2u@studiopress.com', 'Female', 'Randusari', '1977-10-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (105, '35-3143315', '006', 'Paulina', 'Alecock', 'palecock2v@ebay.com', 'Female', 'Malindi', '1996-10-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (106, '71-7904008', '005', 'Lusa', 'Combes', 'lcombes2w@imageshack.us', 'Female', 'Mūsa Khel Bāzār', '1985-04-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (107, '47-4091823', '001', 'Abraham', 'Rotte', 'arotte2x@reddit.com', 'Male', 'Lesnoye', '1982-12-13', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (108, '83-0533427', '002', 'Kamila', 'Leving', 'kleving2y@sphinn.com', 'Female', 'Gobang', '1971-11-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (109, '11-4742411', '003', 'Rolando', 'Handsheart', 'rhandsheart2z@xing.com', 'Male', 'Anshun', '1984-11-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (110, '55-8068713', '005', 'Maxine', 'Adicot', 'madicot30@ted.com', 'Female', 'Örnsköldsvik', '1989-03-09', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (111, '28-9666302', '004', 'Yorke', 'Ferrandez', 'yferrandez31@bizjournals.com', 'Male', 'Paloh', '1991-09-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (112, '95-4368095', '005', 'Timoteo', 'Seth', 'tseth32@yahoo.co.jp', 'Male', 'Ansermanuevo', '1987-06-26', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (113, '07-2456929', '005', 'Harriette', 'Crack', 'hcrack33@purevolume.com', 'Female', 'Nantes', '1989-03-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (114, '31-8229391', '001', 'Anatollo', 'Egan', 'aegan34@t-online.de', 'Male', 'Bagadó', '1984-08-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (115, '49-5277325', '005', 'Tierney', 'McCullouch', 'tmccullouch35@webs.com', 'Female', 'Hexi', '1977-02-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (116, '27-9411083', '004', 'Devinne', 'Dunnett', 'ddunnett36@twitpic.com', 'Female', 'Despotovac', '1975-03-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (117, '23-9240165', '008', 'Wallie', 'Bolitho', 'wbolitho37@intel.com', 'Male', 'Guifeng', '1996-12-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (118, '92-1315296', '007', 'Yetty', 'Robben', 'yrobben38@wufoo.com', 'Female', 'Kaiama', '1991-02-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (119, '52-9128509', '008', 'Eldridge', 'Venables', 'evenables39@archive.org', 'Male', 'Ishikari', '1976-03-14', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (120, '53-1396453', '005', 'Jo-anne', 'Henrion', 'jhenrion3a@taobao.com', 'Female', 'Zemun', '1991-06-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (121, '82-9361300', '001', 'Dolf', 'Spurge', 'dspurge3b@mac.com', 'Male', 'Pita', '1996-12-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (122, '17-3137408', '006', 'Georgie', 'Fishe', 'gfishe3c@amazon.co.uk', 'Male', 'Zacatecoluca', '1995-03-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (123, '80-9774454', '008', 'Nelli', 'Shiell', 'nshiell3d@dagondesign.com', 'Female', 'Benito Juarez', '1977-01-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (124, '21-1055805', '001', 'Baird', 'Hollingsbee', 'bhollingsbee3e@mit.edu', 'Male', 'Penhold', '1973-03-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (125, '75-2700142', '007', 'Audry', 'Blabie', 'ablabie3f@yelp.com', 'Female', 'Zhigalovo', '1979-10-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (126, '59-6196909', '008', 'Sibylle', 'Enevold', 'senevold3g@buzzfeed.com', 'Female', 'Piteå', '1987-06-09', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (127, '86-1083013', '004', 'Laure', 'Laws', 'llaws3h@amazon.com', 'Female', 'Estancia', '1993-08-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (128, '70-1314343', '003', 'Vilhelmina', 'Leachman', 'vleachman3i@ycombinator.com', 'Female', 'Ziroudani', '1981-01-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (129, '80-4379805', '004', 'Kennith', 'ducarme', 'kducarme3j@w3.org', 'Male', '‘Izbat al Burj', '1976-04-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (130, '91-0862761', '003', 'Mose', 'Cunniff', 'mcunniff3k@cyberchimps.com', 'Male', 'Helsingborg', '1984-07-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (131, '98-5272868', '003', 'Paddy', 'Mundy', 'pmundy3l@google.co.uk', 'Male', 'Stockholm', '1996-02-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (132, '04-2627169', '004', 'Reece', 'Georgievski', 'rgeorgievski3m@t.co', 'Male', 'Gujrāt', '1971-09-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (133, '40-4418615', '002', 'Candra', 'Munnings', 'cmunnings3n@over-blog.com', 'Female', 'Consolación del Sur', '1991-06-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (134, '39-6736264', '002', 'Zorine', 'Clymo', 'zclymo3o@surveymonkey.com', 'Female', 'Jacareí', '1974-08-26', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (135, '03-1577505', '001', 'Sauncho', 'MacElholm', 'smacelholm3p@ucla.edu', 'Male', 'Junglinster', '1974-02-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (136, '82-2898153', '005', 'Judy', 'Burleton', 'jburleton3q@oaic.gov.au', 'Female', 'Dukay', '1984-08-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (137, '12-0110280', '007', 'Merrie', 'Pitcaithley', 'mpitcaithley3r@paginegialle.it', 'Female', 'Cherëmukhovo', '1997-08-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (138, '62-9173877', '007', 'Brook', 'Kasbye', 'bkasbye3s@tinyurl.com', 'Female', 'Iporã', '1977-04-12', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (139, '46-0519846', '006', 'Woody', 'Pain', 'wpain3t@livejournal.com', 'Male', 'Leku', '1982-02-12', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (140, '46-1787690', '003', 'Barth', 'Simnett', 'bsimnett3u@reuters.com', 'Male', 'Oroqen Zizhiqi', '1978-08-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (764, '05-8327531', '005', 'Pat', 'Reding', 'predingl6@dot.gov', 'Female', 'Woja', '1971-08-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (141, '08-2093171', '004', 'Ive', 'Sollars', 'isollars3v@e-recht24.de', 'Male', 'Valle de La Pascua', '1978-03-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (142, '17-3237293', '008', 'Raul', 'Kores', 'rkores3w@barnesandnoble.com', 'Male', 'Bonao', '1989-07-19', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (143, '85-2521319', '006', 'Electra', 'Buckeridge', 'ebuckeridge3x@salon.com', 'Female', 'Polazna', '1986-08-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (144, '67-0950787', '004', 'Alberta', 'Pickersail', 'apickersail3y@ning.com', 'Female', 'Auriflama', '1988-08-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (145, '01-7357043', '002', 'Myrle', 'Baal', 'mbaal3z@blogspot.com', 'Female', 'Zekou', '1979-01-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (146, '83-7304125', '007', 'Nevins', 'Crossby', 'ncrossby40@ca.gov', 'Male', 'Jorong', '1998-04-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (147, '46-1837603', '008', 'Tracy', 'Hollyland', 'thollyland41@independent.co.uk', 'Female', 'Quibdó', '1973-08-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (148, '07-0609416', '008', 'Danit', 'Ridoutt', 'dridoutt42@i2i.jp', 'Female', 'Jovellar', '1973-10-29', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (149, '17-9486801', '005', 'Rebeka', 'Jayes', 'rjayes43@weebly.com', 'Female', 'Anwen', '1994-07-18', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (150, '76-4688308', '007', 'Fred', 'Ketchen', 'fketchen44@flickr.com', 'Female', 'Banjar Danginsema', '1996-06-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (151, '80-5966607', '007', 'Ginger', 'Mattiazzo', 'gmattiazzo45@a8.net', 'Male', 'Xunqiao', '1983-09-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (152, '54-1599280', '008', 'Tonie', 'Kezar', 'tkezar46@nationalgeographic.com', 'Female', 'Phimai', '1985-09-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (153, '36-7324565', '001', 'Yance', 'Sandyford', 'ysandyford47@yolasite.com', 'Male', 'Kayanza', '1976-06-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (154, '71-2021909', '001', 'Lizzy', 'Jennaroy', 'ljennaroy48@acquirethisname.com', 'Female', 'Kalpin', '1994-09-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (155, '39-1888803', '003', 'Latia', 'Perello', 'lperello49@google.pl', 'Female', 'Pedreira', '1977-05-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (156, '73-8319926', '005', 'Abrahan', 'Ambrogioli', 'aambrogioli4a@newyorker.com', 'Male', 'Lampitak', '1978-09-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (157, '54-2708264', '001', 'Gunter', 'Moreland', 'gmoreland4b@nature.com', 'Male', 'Curuan', '1971-09-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (158, '84-0970599', '006', 'Alyssa', 'Erridge', 'aerridge4c@comsenz.com', 'Female', 'Maganha', '1991-10-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (159, '58-8674622', '001', 'Vicky', 'Turtle', 'vturtle4d@prweb.com', 'Female', 'Rushanzhai', '1992-07-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (160, '19-6870488', '005', 'Harlene', 'Claque', 'hclaque4e@bravesites.com', 'Female', 'Uppsala', '1989-04-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (161, '69-1601802', '001', 'Wilone', 'Maunsell', 'wmaunsell4f@ycombinator.com', 'Female', 'Liuduzhai', '1990-10-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (162, '25-3217625', '006', 'Bobine', 'Mosconi', 'bmosconi4g@nasa.gov', 'Female', 'Embajador Martini', '1977-06-09', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (163, '26-8582136', '005', 'Wake', 'Grinyov', 'wgrinyov4h@ehow.com', 'Male', 'Zhong’an', '1988-02-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (164, '31-5333752', '003', 'Jan', 'Bankhurst', 'jbankhurst4i@drupal.org', 'Male', 'Hezuoqiao', '1989-01-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (165, '84-8695537', '008', 'Vladamir', 'Vanezis', 'vvanezis4j@tinypic.com', 'Male', 'Gigante', '1977-03-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (166, '19-2407920', '004', 'Estell', 'Maruszewski', 'emaruszewski4k@dyndns.org', 'Female', 'Matagami', '1976-01-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (167, '18-7480330', '007', 'Gustav', 'Conyard', 'gconyard4l@wsj.com', 'Male', 'Noginsk-9', '1989-04-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (168, '80-2828994', '007', 'Lexine', 'Grishankov', 'lgrishankov4m@mail.ru', 'Female', 'San Pedro Apartado', '1987-11-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (169, '26-7068089', '001', 'Xena', 'Paffley', 'xpaffley4n@ameblo.jp', 'Female', 'Melíki', '1987-12-26', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (170, '64-9163948', '001', 'Raf', 'Twell', 'rtwell4o@smh.com.au', 'Female', 'Chang’an', '1978-12-16', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (171, '93-8619659', '006', 'Devin', 'Bigg', 'dbigg4p@marketwatch.com', 'Male', 'Singajaya', '1989-05-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (172, '55-1669770', '006', 'Tarra', 'Richardon', 'trichardon4q@barnesandnoble.com', 'Female', 'Comé', '1987-07-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (173, '04-3356348', '003', 'Salomo', 'Trenbay', 'strenbay4r@cdbaby.com', 'Male', 'Balong', '1993-07-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (174, '81-3817714', '004', 'Welbie', 'Derl', 'wderl4s@nbcnews.com', 'Male', 'Worcester', '1981-12-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (175, '97-0288461', '002', 'Garner', 'Matchett', 'gmatchett4t@globo.com', 'Male', 'Lesozavodsk', '1990-01-06', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (176, '04-3173122', '001', 'Arlin', 'People', 'apeople4u@virginia.edu', 'Male', 'Parigi', '1987-09-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (177, '51-8405318', '006', 'Mahmud', 'Stappard', 'mstappard4v@flavors.me', 'Male', 'Brasília', '1971-05-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (178, '20-6290675', '002', 'Gae', 'Bridge', 'gbridge4w@unicef.org', 'Female', 'Beaverton', '1987-02-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (179, '20-5275453', '006', 'Harwell', 'Gudge', 'hgudge4x@ibm.com', 'Male', 'Vargön', '1979-03-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (180, '36-4288397', '007', 'Karoly', 'Pachmann', 'kpachmann4y@issuu.com', 'Female', 'Bouça', '1990-06-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (181, '25-4950182', '007', 'Fae', 'Tzuker', 'ftzuker4z@netvibes.com', 'Female', 'Jengglungharjo', '1985-04-01', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (182, '88-7325147', '001', 'Hermann', 'Taggart', 'htaggart50@topsy.com', 'Male', 'Sakākā', '1981-09-23', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (183, '49-5631082', '004', 'Jayme', 'Greir', 'jgreir51@hc360.com', 'Male', 'Bereeda', '1983-07-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (184, '52-6121424', '003', 'Jonell', 'Dalliston', 'jdalliston52@feedburner.com', 'Female', 'Flin Flon', '1979-04-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (185, '33-2796045', '008', 'Fidelia', 'Salmond', 'fsalmond53@marriott.com', 'Female', 'Youxi Chengguanzhen', '1988-09-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (186, '17-2301997', '003', 'Putnem', 'Gabbidon', 'pgabbidon54@flickr.com', 'Male', 'Galimuyod', '1994-01-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (187, '46-4160021', '001', 'Jaquith', 'McGrudder', 'jmcgrudder55@barnesandnoble.com', 'Female', 'Kcynia', '1976-12-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (188, '12-9747791', '008', 'Bobbye', 'Marron', 'bmarron56@over-blog.com', 'Female', 'København', '1983-03-31', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (189, '93-0981039', '003', 'Kippy', 'Gherarducci', 'kgherarducci57@issuu.com', 'Male', 'Sanxi', '1994-09-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (190, '60-0922765', '006', 'Trudy', 'Pappi', 'tpappi58@sfgate.com', 'Female', 'Troitsk', '1990-04-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (191, '08-8074032', '008', 'Shep', 'Biaggioli', 'sbiaggioli59@earthlink.net', 'Male', 'Opočno', '1977-08-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (192, '38-6365028', '002', 'Barby', 'Fripp', 'bfripp5a@networksolutions.com', 'Female', 'Sapiranga', '1989-08-13', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (193, '90-9995977', '003', 'Finn', 'Brandi', 'fbrandi5b@addthis.com', 'Male', 'Reutlingen', '1973-06-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (194, '04-3672720', '001', 'Veriee', 'Brozsset', 'vbrozsset5c@feedburner.com', 'Female', 'Qiaosi', '1971-07-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (195, '89-3785286', '008', 'Dorie', 'Joslyn', 'djoslyn5d@wired.com', 'Male', 'Kansas City', '1995-02-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (196, '93-5828580', '003', 'Raddie', 'Seargeant', 'rseargeant5e@harvard.edu', 'Male', 'Ishii', '1998-04-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (197, '69-1279245', '008', 'Emili', 'Rubenczyk', 'erubenczyk5f@netvibes.com', 'Female', 'Na Tan', '1980-02-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (198, '70-6229442', '007', 'Benjie', 'Fried', 'bfried5g@usatoday.com', 'Male', 'Fatufeto', '1985-11-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (199, '45-0827105', '007', 'Carlen', 'Cahillane', 'ccahillane5h@drupal.org', 'Female', 'Livádion', '1984-01-18', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (200, '75-9771160', '007', 'Reynold', 'Allard', 'rallard5i@cdbaby.com', 'Male', 'Alingsås', '1974-10-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (201, '56-9356657', '007', 'Shane', 'Liebmann', 'sliebmann5j@google.pl', 'Female', 'Salaberry-de-Valleyfield', '1992-09-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (202, '79-5544943', '002', 'Heinrik', 'Schoolfield', 'hschoolfield5k@google.ca', 'Male', 'Matão', '1980-02-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (203, '41-4315100', '006', 'Sansone', 'Jewsbury', 'sjewsbury5l@godaddy.com', 'Male', 'Lucaya', '1983-06-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (204, '05-3920735', '002', 'Benoit', 'Priscott', 'bpriscott5m@usgs.gov', 'Male', 'Shushicë', '1997-10-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (205, '51-4322969', '005', 'Boot', 'Serjeant', 'bserjeant5n@japanpost.jp', 'Male', 'Radiměř', '1990-06-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (206, '62-8262497', '002', 'Babbette', 'Tuson', 'btuson5o@soundcloud.com', 'Female', 'Sukorejo', '1984-05-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (207, '61-4756168', '008', 'Giorgio', 'Konig', 'gkonig5p@fema.gov', 'Male', 'Andovoranto', '1974-10-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (208, '63-4602272', '001', 'Baxie', 'Luis', 'bluis5q@digg.com', 'Male', 'Voronkov', '1973-05-06', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (209, '35-5029239', '002', 'Jasper', 'Stockau', 'jstockau5r@amazon.co.jp', 'Male', 'Wachira Barami', '1979-04-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (210, '97-9271961', '007', 'Viviene', 'Hillatt', 'vhillatt5s@tuttocitta.it', 'Female', 'Vidyayevo', '1990-04-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (211, '29-2191162', '004', 'Toddie', 'Pyner', 'tpyner5t@nymag.com', 'Male', 'Dakhla', '1980-02-24', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (212, '77-5043151', '002', 'Vi', 'Schultze', 'vschultze5u@naver.com', 'Female', 'Črenšovci', '1980-03-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (213, '51-1433281', '007', 'Teodoor', 'Thoday', 'tthoday5v@arizona.edu', 'Male', 'Heicheng', '1978-01-01', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (214, '43-9122919', '008', 'Benedetta', 'Clother', 'bclother5w@google.com.hk', 'Female', 'Guarda Nova', '1997-05-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (215, '17-2163466', '006', 'Yetty', 'Collinson', 'ycollinson5x@seattletimes.com', 'Female', 'Lyaskovets', '1982-01-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (216, '76-1085924', '003', 'Joy', 'Behneke', 'jbehneke5y@liveinternet.ru', 'Female', 'Aranhas', '1980-08-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (217, '01-3335857', '004', 'Gretchen', 'Ladyman', 'gladyman5z@freewebs.com', 'Female', 'Igurusi', '1986-01-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (218, '36-2248159', '003', 'Lucio', 'Marskell', 'lmarskell60@home.pl', 'Male', 'Nueva Cajamarca', '1972-08-29', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (219, '74-1798584', '002', 'Jaquelin', 'Haggidon', 'jhaggidon61@skyrock.com', 'Female', 'Gémeos', '1994-04-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (220, '67-8999145', '005', 'Roseanna', 'Pole', 'rpole62@nydailynews.com', 'Female', 'Jhumra', '1986-07-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (221, '84-6127799', '004', 'Matthaeus', 'Germaine', 'mgermaine63@tripadvisor.com', 'Male', 'Jinqiao', '1995-08-04', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (222, '80-1508325', '003', 'Elnar', 'Ochterlony', 'eochterlony64@list-manage.com', 'Male', 'Verona', '1989-06-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (223, '96-7202168', '007', 'Meade', 'Warlawe', 'mwarlawe65@usgs.gov', 'Female', 'Caringin', '1989-08-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (224, '55-6312489', '006', 'Uriah', 'Rhydderch', 'urhydderch66@seattletimes.com', 'Male', 'Vila Boa do Bispo', '1985-07-21', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (225, '92-3001356', '006', 'Papagena', 'Autrie', 'pautrie67@washingtonpost.com', 'Female', 'Przyborów', '1973-01-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (226, '68-4596863', '004', 'Kim', 'Gehrts', 'kgehrts68@51.la', 'Female', 'Piquete', '1997-05-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (227, '41-0522588', '008', 'Rois', 'Caslin', 'rcaslin69@cloudflare.com', 'Female', 'San Carlos', '1981-06-11', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (228, '35-8432834', '004', 'Jenelle', 'Baldock', 'jbaldock6a@privacy.gov.au', 'Female', 'Shangshuai', '1994-09-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (229, '29-0557394', '002', 'Chester', 'Kilcoyne', 'ckilcoyne6b@sakura.ne.jp', 'Male', 'Florencia', '1986-02-16', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (230, '31-6280913', '003', 'Esther', 'Andrick', 'eandrick6c@paypal.com', 'Female', 'Taouloukoult', '1994-07-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (231, '45-6595709', '006', 'Layney', 'Mynott', 'lmynott6d@earthlink.net', 'Female', 'Igarra', '1974-06-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (232, '72-9473755', '008', 'Denys', 'Denge', 'ddenge6e@npr.org', 'Male', 'Estância Velha', '1996-05-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (233, '95-1892661', '008', 'Marilin', 'Beckingham', 'mbeckingham6f@washingtonpost.com', 'Female', 'Tangerang', '1981-08-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (234, '72-8547946', '008', 'Kandy', 'Bellard', 'kbellard6g@cdbaby.com', 'Female', 'Picos', '1997-06-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (235, '20-0740774', '008', 'Bord', 'Petrelli', 'bpetrelli6h@va.gov', 'Male', 'Jinan', '1972-04-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (236, '54-0186420', '007', 'Rob', 'Stonham', 'rstonham6i@ucla.edu', 'Male', 'Wuluo', '1985-03-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (237, '76-3753237', '008', 'Monty', 'Custard', 'mcustard6j@oaic.gov.au', 'Male', 'Nizhnyaya Omka', '1992-05-18', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (238, '41-6843220', '004', 'Ilaire', 'Foston', 'ifoston6k@samsung.com', 'Male', 'Essen', '1996-05-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (239, '23-6335920', '008', 'Bard', 'O''Hegertie', 'bohegertie6l@vimeo.com', 'Male', 'Mashan', '1985-12-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (240, '95-6771280', '001', 'Frasier', 'Shortland', 'fshortland6m@latimes.com', 'Male', 'Vinha', '1978-07-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (241, '07-5593726', '001', 'Janifer', 'Quipp', 'jquipp6n@ucoz.ru', 'Female', 'Damai', '1975-04-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (242, '50-1474499', '001', 'Twyla', 'Eykelbosch', 'teykelbosch6o@constantcontact.com', 'Female', 'Cruz', '1971-05-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (243, '72-9170325', '005', 'Elnore', 'Gammel', 'egammel6p@jugem.jp', 'Female', 'Yanhe', '1987-03-21', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (244, '93-8333845', '008', 'Gretal', 'Thumim', 'gthumim6q@walmart.com', 'Female', 'Xinqiao', '1978-01-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (245, '08-8922947', '008', 'Randy', 'Grigori', 'rgrigori6r@cbslocal.com', 'Female', 'Hongguang', '1990-10-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (246, '57-9611537', '006', 'Mead', 'Hauch', 'mhauch6s@google.nl', 'Male', 'Oeba', '1993-08-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (247, '16-6396907', '007', 'Mollee', 'Haselhurst', 'mhaselhurst6t@zdnet.com', 'Female', 'Xiangcunxiang', '1972-07-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (248, '47-4458214', '003', 'Pepita', 'Hedling', 'phedling6u@qq.com', 'Female', 'Tebingtinggi', '1977-04-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (249, '73-0164327', '008', 'Laureen', 'Gulvin', 'lgulvin6v@eepurl.com', 'Female', 'Dayan', '1973-09-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (250, '98-5889087', '002', 'Kinny', 'Muggleton', 'kmuggleton6w@theguardian.com', 'Male', 'Blantyre', '1991-08-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (251, '51-9062948', '001', 'Forrester', 'Eslie', 'feslie6x@toplist.cz', 'Male', 'Dijon', '1989-07-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (252, '14-4274482', '002', 'Maryanne', 'Mehaffey', 'mmehaffey6y@businessinsider.com', 'Female', 'Kansas City', '1979-11-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (253, '85-1015743', '005', 'Crista', 'Saltern', 'csaltern6z@fastcompany.com', 'Female', 'Hepo', '1977-03-15', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (254, '47-8050648', '001', 'Eldredge', 'Alvis', 'ealvis70@drupal.org', 'Male', 'Longmen', '1996-04-18', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (255, '83-9334438', '001', 'Monroe', 'Wadsworth', 'mwadsworth71@nasa.gov', 'Male', 'Krouna', '1975-06-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (256, '98-7078433', '005', 'Danny', 'Behan', 'dbehan72@photobucket.com', 'Male', 'Huanshan', '1992-11-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (257, '75-3703029', '002', 'Wolfy', 'Radbone', 'wradbone73@chicagotribune.com', 'Male', 'Jangheung', '1974-06-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (258, '97-4121808', '005', 'Diann', 'Merrell', 'dmerrell74@myspace.com', 'Female', 'San Carlos', '1976-05-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (259, '02-5366798', '005', 'Hilary', 'Beadle', 'hbeadle75@fotki.com', 'Female', 'Guhuai', '1987-11-19', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (260, '15-5254228', '006', 'Orbadiah', 'Castle', 'ocastle76@cloudflare.com', 'Male', 'Bengubelan', '1984-09-19', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (261, '23-6996765', '006', 'Barrie', 'Hallut', 'bhallut77@goodreads.com', 'Female', 'Ngembul', '1980-05-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (262, '23-9288483', '002', 'Marrissa', 'Egar', 'megar78@reuters.com', 'Female', 'Jieguanting', '1984-12-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (263, '84-7488382', '008', 'Thedric', 'Udey', 'tudey79@virginia.edu', 'Male', 'Chang’an', '1997-11-14', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (264, '21-6611312', '005', 'Carolina', 'Blakeden', 'cblakeden7a@webs.com', 'Female', 'Austin', '1977-08-01', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (265, '48-8624777', '004', 'Cyndy', 'Starling', 'cstarling7b@nydailynews.com', 'Female', 'Gaiyang', '1991-06-19', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (266, '78-5002110', '004', 'Ringo', 'Adolfsen', 'radolfsen7c@friendfeed.com', 'Male', 'Iwaki', '1995-04-06', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (267, '67-4568201', '006', 'Enoch', 'MacRannell', 'emacrannell7d@marriott.com', 'Male', 'Krayen', '1989-07-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (268, '68-2153562', '005', 'Kirstin', 'Batchellor', 'kbatchellor7e@rediff.com', 'Female', 'Az Zaytūnīyah', '1991-09-16', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (269, '77-6625545', '007', 'Gloriana', 'Ennor', 'gennor7f@themeforest.net', 'Female', 'Dřevohostice', '1987-12-09', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (270, '27-1526980', '008', 'Jobye', 'Dake', 'jdake7g@blogspot.com', 'Female', 'Hronov', '1973-11-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (271, '67-6711295', '007', 'Darrel', 'Dunkirk', 'ddunkirk7h@oakley.com', 'Male', 'Nesebar', '1984-09-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (272, '35-3942620', '008', 'Monte', 'Salmond', 'msalmond7i@github.com', 'Male', 'La Ligua', '1983-06-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (273, '53-0086903', '008', 'Addia', 'Aldhouse', 'aaldhouse7j@miibeian.gov.cn', 'Female', 'Xinghua', '1981-11-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (274, '43-6126685', '005', 'Moll', 'Brideaux', 'mbrideaux7k@home.pl', 'Female', 'Kungsbacka', '1981-07-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (275, '35-4985684', '001', 'Phylys', 'Morrill', 'pmorrill7l@redcross.org', 'Female', 'Panitian', '1982-03-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (276, '36-9566139', '005', 'Kingston', 'Godier', 'kgodier7m@washingtonpost.com', 'Male', 'Kurumkan', '1972-09-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (277, '18-2648900', '008', 'Matti', 'Gerber', 'mgerber7n@smugmug.com', 'Female', 'Lyon', '1975-12-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (278, '32-7533014', '001', 'Clem', 'Shand', 'cshand7o@java.com', 'Male', 'Zhaowanzhuang', '1993-05-14', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (279, '54-4897191', '008', 'Niel', 'Mulvaney', 'nmulvaney7p@webs.com', 'Male', 'Abuyog', '1973-01-12', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (280, '71-6296406', '005', 'Ronnie', 'Heining', 'rheining7q@google.ca', 'Male', 'Omsukchan', '1975-02-16', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (281, '17-1342549', '008', 'Lowrance', 'Droghan', 'ldroghan7r@reuters.com', 'Male', 'Burūm', '1979-12-29', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (282, '60-7777283', '007', 'Wilmer', 'Hannon', 'whannon7s@deliciousdays.com', 'Male', 'Payxambabazar', '1975-06-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (283, '85-3684471', '004', 'Andrej', 'Axel', 'aaxel7t@miibeian.gov.cn', 'Male', 'Korogan Timur', '1986-12-23', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (284, '23-5079718', '002', 'Maxie', 'Skippen', 'mskippen7u@ebay.com', 'Male', 'Tuy Phước', '1979-12-09', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (285, '72-7558578', '003', 'Roxanna', 'Poplee', 'rpoplee7v@spiegel.de', 'Female', 'Ngaruawahia', '1987-12-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (286, '55-8902793', '001', 'Joanne', 'Batecok', 'jbatecok7w@google.com.br', 'Female', 'Fundación', '1979-07-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (287, '01-9253200', '008', 'Isadore', 'Tuddenham', 'ituddenham7x@mlb.com', 'Male', 'Yala', '1982-06-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (288, '25-3051516', '003', 'Kipp', 'Follitt', 'kfollitt7y@google.nl', 'Male', 'Niutang', '1988-11-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (289, '83-3395406', '006', 'Gertrudis', 'Cheston', 'gcheston7z@etsy.com', 'Female', 'Simo', '1994-02-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (290, '45-4678040', '002', 'Almeda', 'Jachtym', 'ajachtym80@myspace.com', 'Female', 'Encontrados', '1974-05-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (291, '72-1631908', '001', 'Livvyy', 'Leyzell', 'lleyzell81@patch.com', 'Female', 'Sim', '1995-03-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (292, '49-8218723', '006', 'Dyana', 'Sinfield', 'dsinfield82@clickbank.net', 'Female', 'Irkliyevskaya', '1988-01-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (293, '49-4837986', '008', 'Dara', 'Salvage', 'dsalvage83@4shared.com', 'Female', 'Xiangshan', '1988-04-10', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (294, '76-7096653', '003', 'Kathi', 'McIllrick', 'kmcillrick84@ehow.com', 'Female', 'Corozal', '1993-12-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (295, '89-6579157', '006', 'Rusty', 'Henderson', 'rhenderson85@xing.com', 'Male', 'Yuqunweng', '1997-09-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (296, '42-9674674', '005', 'Joachim', 'Drewe', 'jdrewe86@1und1.de', 'Male', 'Londrina', '1987-04-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (297, '54-9478272', '006', 'Percival', 'Parffrey', 'pparffrey87@163.com', 'Male', 'Mountain View', '1996-04-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (298, '07-2591554', '001', 'Bella', 'Barkaway', 'bbarkaway88@google.com.br', 'Female', 'Sumy', '1984-05-24', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (299, '27-1152983', '007', 'Sacha', 'Lattey', 'slattey89@photobucket.com', 'Female', 'Shatian', '1984-12-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (300, '34-7696480', '007', 'Stacee', 'Alner', 'salner8a@japanpost.jp', 'Male', 'Muurame', '1997-07-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (301, '48-7071473', '003', 'Jordan', 'Fazzioli', 'jfazzioli8b@ifeng.com', 'Female', 'Buenaventura', '1975-02-07', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (302, '31-2701220', '002', 'Teddi', 'Timmis', 'ttimmis8c@usnews.com', 'Female', 'Dongmazar', '1988-06-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (303, '90-4893166', '007', 'Stacy', 'Pigeram', 'spigeram8d@list-manage.com', 'Male', 'Madan', '1997-06-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (304, '29-8717300', '008', 'Rivi', 'Hudd', 'rhudd8e@mozilla.org', 'Female', 'An Nāşirah', '1972-09-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (305, '69-9783180', '002', 'Haily', 'Ridings', 'hridings8f@google.it', 'Female', 'Oeri’u', '1978-02-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (306, '81-0018997', '003', 'Salomo', 'Sindell', 'ssindell8g@si.edu', 'Male', 'Yuci', '1974-01-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (307, '16-4119514', '001', 'Denny', 'Pykerman', 'dpykerman8h@bloglovin.com', 'Male', 'Mawang', '1993-02-07', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (308, '74-7591059', '003', 'Bone', 'Couthard', 'bcouthard8i@sciencedirect.com', 'Male', 'Nässjö', '1993-08-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (309, '84-6872129', '001', 'Dianna', 'Rief', 'drief8j@devhub.com', 'Female', 'Chemodanovka', '1971-10-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (310, '74-2557260', '008', 'Floria', 'Tocknell', 'ftocknell8k@wordpress.org', 'Female', 'Bragança', '1979-09-21', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (311, '83-8685846', '005', 'Dawn', 'Mosdell', 'dmosdell8l@blinklist.com', 'Female', 'Dongzhou', '1987-07-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (312, '60-4545117', '004', 'Kaspar', 'Blanning', 'kblanning8m@msn.com', 'Male', 'Alfenas', '1978-04-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (313, '29-3499013', '006', 'Rhody', 'Waulker', 'rwaulker8n@irs.gov', 'Female', 'Hexi', '1996-07-11', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (314, '11-9696480', '004', 'Trent', 'Nelson', 'tnelson8o@hhs.gov', 'Male', 'Almere Haven', '1974-04-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (315, '54-0802397', '008', 'Vinnie', 'Rowsel', 'vrowsel8p@virginia.edu', 'Female', 'Donan', '1983-07-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (316, '21-5808074', '003', 'Laryssa', 'Lamanby', 'llamanby8q@elegantthemes.com', 'Female', 'Derzhavīnsk', '1983-03-01', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (317, '96-1773681', '003', 'Armin', 'Barraclough', 'abarraclough8r@disqus.com', 'Male', 'Malax', '1993-03-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (318, '85-7128188', '002', 'Emmanuel', 'Battye', 'ebattye8s@sogou.com', 'Male', 'Hengzhou', '1985-05-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (319, '99-4214673', '002', 'Bernardina', 'Tellenbach', 'btellenbach8t@weibo.com', 'Female', 'Ulashan', '1981-06-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (320, '61-2550384', '002', 'Gilberta', 'Baudry', 'gbaudry8u@miitbeian.gov.cn', 'Female', 'Zhangjiawan', '1977-08-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (321, '16-3345028', '005', 'Tony', 'Beebee', 'tbeebee8v@etsy.com', 'Male', 'København', '1985-09-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (322, '37-7177522', '001', 'Kristoforo', 'Abramowsky', 'kabramowsky8w@shinystat.com', 'Male', 'Rajal Norte', '1988-05-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (323, '14-6164885', '005', 'Corbet', 'Redihough', 'credihough8x@netvibes.com', 'Male', 'Madaoua', '1984-04-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (324, '60-6314005', '006', 'Nataline', 'Harm', 'nharm8y@shutterfly.com', 'Female', 'Naga', '1981-11-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (325, '70-9724916', '004', 'Kylen', 'Farnorth', 'kfarnorth8z@goo.gl', 'Female', 'Shangzhang', '1981-06-12', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (326, '00-4785219', '004', 'Conni', 'Van den Bosch', 'cvandenbosch90@seattletimes.com', 'Female', 'Liangbing', '1972-11-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (327, '35-0293326', '002', 'Goddart', 'Rolfo', 'grolfo91@hatena.ne.jp', 'Male', 'Ibotirama', '1985-03-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (328, '02-5152464', '002', 'Clotilda', 'Cottage', 'ccottage92@ihg.com', 'Female', 'Rongxi', '1991-03-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (329, '95-0281974', '007', 'Dar', 'Truitt', 'dtruitt93@ask.com', 'Male', 'Gävle', '1993-09-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (330, '04-4454840', '004', 'Valdemar', 'Josefson', 'vjosefson94@squarespace.com', 'Male', 'Bački Petrovac', '1991-04-04', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (331, '31-4057861', '001', 'Tobin', 'Cosford', 'tcosford95@elpais.com', 'Male', 'Rathnew', '1980-08-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (332, '88-2507015', '006', 'Rudolph', 'Twelve', 'rtwelve96@symantec.com', 'Male', 'Longbei', '1995-04-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (333, '90-0150987', '003', 'Roldan', 'Rany', 'rrany97@samsung.com', 'Male', 'Al Tuḩaytā’', '1980-09-04', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (334, '51-9457088', '007', 'Kylie', 'Gammel', 'kgammel98@upenn.edu', 'Female', 'Ülken', '1991-09-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (335, '42-7802301', '003', 'Weidar', 'Kirkbright', 'wkirkbright99@youtube.com', 'Male', 'Bin Yauri', '1974-11-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (336, '45-3758022', '007', 'Vail', 'Borthram', 'vborthram9a@vistaprint.com', 'Male', 'Kosiv', '1975-08-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (337, '70-7468289', '002', 'Sunny', 'Linneman', 'slinneman9b@exblog.jp', 'Female', 'Las Varillas', '1987-04-05', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (338, '22-4675055', '002', 'Trude', 'Trethewey', 'ttrethewey9c@howstuffworks.com', 'Female', 'Bukowiec', '1995-11-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (339, '29-4570046', '008', 'Lalo', 'Oslar', 'loslar9d@hugedomains.com', 'Male', 'Kidal', '1990-01-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (340, '26-8821696', '002', 'Titus', 'Scoggans', 'tscoggans9e@illinois.edu', 'Male', 'Lyubotyn', '1988-12-10', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (341, '15-1636358', '002', 'Arda', 'Harlick', 'aharlick9f@archive.org', 'Female', 'Shar’ya', '1986-04-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (342, '77-9744534', '004', 'Raynell', 'Darby', 'rdarby9g@delicious.com', 'Female', 'Colquechaca', '1972-03-13', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (343, '03-8578565', '004', 'Margarete', 'Davitashvili', 'mdavitashvili9h@networkadvertising.org', 'Female', 'Thạnh Mỹ', '1987-10-30', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (344, '13-7342597', '007', 'Liz', 'Spick', 'lspick9i@wufoo.com', 'Female', 'Spas', '1992-01-23', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (345, '88-5983708', '004', 'Mano', 'Cheavin', 'mcheavin9j@spotify.com', 'Male', 'Porirua', '1992-06-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (346, '41-2106020', '007', 'Gnni', 'Chark', 'gchark9k@cornell.edu', 'Female', 'Nancang', '1976-09-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (347, '11-0028592', '008', 'Orton', 'Slessor', 'oslessor9l@diigo.com', 'Male', 'Reserva', '1976-10-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (348, '23-0662669', '001', 'Arel', 'Lemery', 'alemery9m@buzzfeed.com', 'Male', 'Sacramento', '1981-05-06', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (349, '03-2158223', '006', 'Gavin', 'Varley', 'gvarley9n@smh.com.au', 'Male', 'Oakland', '1993-02-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (350, '07-9232511', '005', 'Erie', 'Lacy', 'elacy9o@apache.org', 'Male', 'Mizi', '1981-03-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (351, '78-5633904', '007', 'Ronalda', 'Solman', 'rsolman9p@abc.net.au', 'Female', 'Chashan', '1984-02-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (352, '99-8170270', '007', 'Amil', 'O''Dare', 'aodare9q@pen.io', 'Female', 'Esperanza', '1996-07-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (353, '24-8299267', '005', 'Brock', 'Fuller', 'bfuller9r@adobe.com', 'Male', 'Qushi’an', '1996-06-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (354, '55-7584312', '007', 'Melanie', 'Cesco', 'mcesco9s@list-manage.com', 'Female', 'Guadalupe', '1986-11-09', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (355, '07-7168748', '002', 'Nananne', 'Keywood', 'nkeywood9t@opensource.org', 'Female', 'Miras', '1982-09-25', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (356, '57-8139268', '006', 'Irvin', 'Criple', 'icriple9u@hud.gov', 'Male', 'Kafr Baţnā', '1979-12-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (357, '62-4658565', '005', 'Carlie', 'Bush', 'cbush9v@wisc.edu', 'Male', 'Cibolek Kidul', '1975-10-04', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (358, '42-4089926', '003', 'Felizio', 'Geleman', 'fgeleman9w@ow.ly', 'Male', 'Bajia', '1987-03-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (359, '94-3342780', '002', 'Brooks', 'Jeffray', 'bjeffray9x@pcworld.com', 'Male', 'Xiaoduchuan', '1988-06-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (360, '70-4111225', '001', 'Fernando', 'Prin', 'fprin9y@netvibes.com', 'Male', 'Katsina-Ala', '1982-10-04', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (361, '84-3862987', '003', 'Ernesta', 'Jansey', 'ejansey9z@pbs.org', 'Female', 'Kabalen', '1991-08-23', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (362, '69-4340790', '002', 'Nanete', 'Safhill', 'nsafhilla0@squarespace.com', 'Female', 'Alcabideche', '1980-04-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (363, '61-1240655', '008', 'Lisabeth', 'Coryndon', 'lcoryndona1@dyndns.org', 'Female', 'Wurigelebur', '1987-01-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (364, '74-7073745', '007', 'Meggie', 'Keher', 'mkehera2@hibu.com', 'Female', 'Virginia', '1985-01-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (365, '75-9461055', '001', 'Gregor', 'Theobalds', 'gtheobaldsa3@hatena.ne.jp', 'Male', 'Ryjewo', '1980-05-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (366, '93-7209946', '005', 'Johanna', 'de Copeman', 'jdecopemana4@storify.com', 'Female', 'Young America', '1973-04-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (367, '15-6590662', '002', 'Alexandr', 'Schuelcke', 'aschuelckea5@gnu.org', 'Male', 'Cipondok', '1983-05-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (368, '15-4101496', '003', 'Kele', 'Salway', 'ksalwaya6@umich.edu', 'Male', 'Vouani', '1996-05-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (369, '03-8259748', '006', 'Iggy', 'Corteis', 'icorteisa7@microsoft.com', 'Male', 'Cotuí', '1973-10-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (370, '67-4804219', '003', 'Natividad', 'Zoellner', 'nzoellnera8@bravesites.com', 'Female', 'Cipolletti', '1976-11-14', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (371, '43-3101829', '003', 'Bobbee', 'Clouston', 'bcloustona9@guardian.co.uk', 'Female', 'Rusip', '1985-08-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (372, '59-6881293', '006', 'Abbe', 'Hatley', 'ahatleyaa@ftc.gov', 'Female', 'Jibiya', '1988-07-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (373, '36-1561864', '003', 'Carolan', 'Gowanson', 'cgowansonab@icq.com', 'Female', 'Cercal', '1971-09-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (374, '39-9005924', '003', 'Catlin', 'Crowther', 'ccrowtherac@who.int', 'Female', 'Messina', '1988-12-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (375, '13-8629953', '007', 'Bengt', 'Caldero', 'bcalderoad@go.com', 'Male', 'Nieborów', '1983-02-09', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (376, '52-2048178', '002', 'Mattheus', 'Mose', 'mmoseae@sciencedirect.com', 'Male', 'Couva', '1991-11-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (377, '26-2792234', '006', 'Gibby', 'Lorinez', 'glorinezaf@free.fr', 'Male', 'Mariana', '1996-11-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (378, '60-6409513', '005', 'Lorianne', 'Miliffe', 'lmiliffeag@ycombinator.com', 'Female', 'San Buenaventura', '1996-12-26', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (379, '74-6187851', '004', 'Obie', 'Durban', 'odurbanah@prlog.org', 'Male', 'Darnah', '1974-10-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (380, '77-1133272', '004', 'Miguela', 'Dirkin', 'mdirkinai@acquirethisname.com', 'Female', 'Tinabogan', '1982-12-14', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (381, '87-4291271', '002', 'Addison', 'McGoon', 'amcgoonaj@samsung.com', 'Male', 'Zator', '1975-10-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (382, '48-3319607', '006', 'Winny', 'Painswick', 'wpainswickak@weebly.com', 'Male', 'Daliuhao', '1992-10-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (383, '96-5399694', '006', 'Reggi', 'Thrasher', 'rthrasheral@hexun.com', 'Female', 'Staryye Atagi', '1989-12-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (384, '84-1821575', '003', 'Christal', 'Hourstan', 'chourstanam@about.com', 'Female', 'Cerro Colorado', '1992-05-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (385, '36-3253601', '002', 'Carlene', 'Goolden', 'cgooldenan@slideshare.net', 'Female', 'Qiryat Motzkin', '1978-10-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (386, '79-0352335', '008', 'Merrill', 'Russan', 'mrussanao@hhs.gov', 'Male', 'Kastav', '1985-06-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (387, '25-5027804', '003', 'Beitris', 'Zamudio', 'bzamudioap@privacy.gov.au', 'Female', 'Rio Bonito', '1983-10-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (388, '41-1277792', '008', 'Amalia', 'Heffer', 'ahefferaq@ebay.co.uk', 'Female', 'Almargem do Bispo', '1972-06-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (389, '78-3900402', '002', 'Hercules', 'Ruter', 'hruterar@cdbaby.com', 'Male', 'Tân Châu', '1984-02-19', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (390, '40-0544471', '003', 'Eben', 'Haime', 'ehaimeas@alexa.com', 'Male', 'Rotterdam postbusnummers', '1978-11-09', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (391, '87-4312801', '007', 'Godwin', 'McInulty', 'gmcinultyat@wisc.edu', 'Male', 'Anchang', '1975-05-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (392, '59-0873256', '001', 'Oby', 'Gillbard', 'ogillbardau@dmoz.org', 'Male', 'Atlanta', '1992-12-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (393, '57-2078663', '003', 'Elita', 'Abbie', 'eabbieav@sfgate.com', 'Female', 'Hukeng', '1984-12-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (394, '27-0597795', '002', 'Lenna', 'Louiset', 'llouisetaw@oakley.com', 'Female', 'Keroka', '1985-09-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (395, '32-8170351', '005', 'Agnesse', 'Kubicka', 'akubickaax@thetimes.co.uk', 'Female', 'Karasgede', '1994-12-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (396, '11-8547464', '001', 'Gardner', 'Diwell', 'gdiwellay@seesaa.net', 'Male', 'Los Andes', '1988-07-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (397, '06-8376123', '005', 'Hannis', 'Nacey', 'hnaceyaz@ibm.com', 'Female', 'Avignon', '1985-01-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (398, '66-8484972', '006', 'Benedick', 'Hallgalley', 'bhallgalleyb0@myspace.com', 'Male', 'Batgrām', '1978-07-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (399, '99-4664958', '001', 'Mala', 'Spollen', 'mspollenb1@arizona.edu', 'Female', 'Shuishaping', '1985-04-10', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (400, '31-0053236', '003', 'Colline', 'Sancto', 'csanctob2@ted.com', 'Female', 'Dongsheng', '1986-05-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (401, '82-7831114', '002', 'Ursa', 'Matz', 'umatzb3@spotify.com', 'Female', 'La Roche-sur-Yon', '1991-08-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (402, '76-5696948', '004', 'Katrine', 'Kerfut', 'kkerfutb4@nifty.com', 'Female', 'Aras-asan', '1977-03-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (403, '62-2383724', '005', 'Floria', 'Brusin', 'fbrusinb5@amazon.com', 'Female', 'Colcabamba', '1985-07-11', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (404, '29-0207419', '002', 'Joyous', 'Cannaway', 'jcannawayb6@about.me', 'Female', 'Obertyn', '1990-12-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (405, '28-3317766', '004', 'Faythe', 'Sictornes', 'fsictornesb7@pinterest.com', 'Female', 'Komiža', '1973-01-30', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (406, '91-1811008', '007', 'Toma', 'Somerscales', 'tsomerscalesb8@sakura.ne.jp', 'Female', 'San Felipe', '1976-05-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (407, '63-2535865', '002', 'Moira', 'Eilles', 'meillesb9@yale.edu', 'Female', 'Al Masallamiyya', '1974-03-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (408, '94-8582300', '005', 'Zack', 'Jansa', 'zjansaba@printfriendly.com', 'Male', 'Badou', '1979-04-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (409, '10-1444281', '002', 'Barbi', 'Dulwitch', 'bdulwitchbb@fda.gov', 'Female', 'Ban Phan Don', '1984-03-16', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (410, '70-8119480', '008', 'Linnea', 'Gon', 'lgonbc@europa.eu', 'Female', 'Koukkuniemi', '1973-09-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (411, '85-6231456', '006', 'Zsazsa', 'Pass', 'zpassbd@cnbc.com', 'Female', 'Cullhuas', '1995-10-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (412, '63-9056786', '003', 'Kati', 'Arondel', 'karondelbe@reddit.com', 'Female', 'Seulimeum', '1987-09-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (413, '71-8943830', '008', 'Aeriel', 'Woodgate', 'awoodgatebf@usa.gov', 'Female', 'Qarqaraly', '1971-09-22', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (414, '78-5917064', '005', 'Onofredo', 'Dorbon', 'odorbonbg@vkontakte.ru', 'Male', 'Puerto Rico', '1985-10-22', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (415, '05-2583199', '001', 'Jozef', 'Petroulis', 'jpetroulisbh@cbc.ca', 'Male', 'Cestas', '1974-08-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (416, '05-0915830', '006', 'Niccolo', 'Iacovuzzi', 'niacovuzzibi@stumbleupon.com', 'Male', 'Mandeman Daya', '1978-03-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (417, '25-8226054', '003', 'Meghan', 'Winspurr', 'mwinspurrbj@marriott.com', 'Female', 'Sumberkenanga', '1990-12-19', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (418, '28-4637509', '002', 'Hurleigh', 'Tolland', 'htollandbk@sina.com.cn', 'Male', 'Xiaojie', '1971-12-26', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (419, '56-6881169', '005', 'Drake', 'Whitlock', 'dwhitlockbl@mtv.com', 'Male', 'Port Area', '1976-11-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (420, '65-8701650', '002', 'Odie', 'Keeble', 'okeeblebm@cdbaby.com', 'Male', 'Barbaza', '1995-09-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (421, '69-7522367', '002', 'Adda', 'Bellee', 'abelleebn@wikia.com', 'Female', 'Miskolc', '1992-04-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (422, '15-6210434', '004', 'Donnamarie', 'Osboldstone', 'dosboldstonebo@dmoz.org', 'Female', 'Petrivka', '1985-10-20', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (423, '10-8600137', '004', 'Vonny', 'Kitchingman', 'vkitchingmanbp@istockphoto.com', 'Female', 'Ban Rangsit', '1997-05-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (424, '29-7592439', '001', 'Querida', 'Buller', 'qbullerbq@nifty.com', 'Female', 'Adelaide Mail Centre', '1976-10-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (425, '92-6135284', '005', 'Cari', 'Kubes', 'ckubesbr@skype.com', 'Female', 'Sechelt', '1975-01-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (426, '51-7530682', '006', 'Candice', 'Ouldcott', 'couldcottbs@bluehost.com', 'Female', 'Tianjin', '1971-10-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (427, '03-7009333', '008', 'Jenifer', 'Eldershaw', 'jeldershawbt@dropbox.com', 'Female', 'Sebasang', '1996-09-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (428, '57-6681389', '001', 'Gene', 'Molian', 'gmolianbu@webeden.co.uk', 'Male', 'Gbadolite', '1976-03-16', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (429, '11-9791007', '003', 'Scotti', 'Incogna', 'sincognabv@cbc.ca', 'Male', 'Yonggu', '1993-02-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (430, '04-7874605', '008', 'Felizio', 'Bourgeois', 'fbourgeoisbw@last.fm', 'Male', 'Karangwungu Lor', '1986-07-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (431, '39-1922048', '005', 'Nevins', 'Wilfling', 'nwilflingbx@bloglines.com', 'Male', 'Darkovice', '1978-02-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (432, '62-7914424', '004', 'Kurtis', 'Iacapucci', 'kiacapucciby@phoca.cz', 'Male', 'Bakan', '1989-09-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (433, '80-1903129', '001', 'Shelby', 'Erdes', 'serdesbz@businessinsider.com', 'Male', 'Slavkov u Brna', '1985-06-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (434, '91-0271825', '004', 'Imojean', 'Snailham', 'isnailhamc0@bluehost.com', 'Female', 'Umburarameha', '1973-10-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (435, '33-4721354', '008', 'Karry', 'Danielian', 'kdanielianc1@intel.com', 'Female', 'Linglu', '1985-10-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (436, '82-5608314', '002', 'Blithe', 'Hampstead', 'bhampsteadc2@discovery.com', 'Female', 'Ambatondrazaka', '1975-08-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (437, '45-9859825', '006', 'Florrie', 'Welsby', 'fwelsbyc3@smh.com.au', 'Female', 'Malaga', '1981-06-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (438, '27-2930809', '003', 'Karim', 'Soall', 'ksoallc4@sourceforge.net', 'Male', 'Canhestros', '1982-12-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (439, '72-7968712', '003', 'Kaela', 'Pashe', 'kpashec5@miitbeian.gov.cn', 'Female', 'Vihāri', '1971-05-06', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (440, '17-6527495', '005', 'Wally', 'Burnsell', 'wburnsellc6@yellowpages.com', 'Female', 'Buang', '1977-05-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (441, '33-0173252', '007', 'Jaquenette', 'Lingner', 'jlingnerc7@netlog.com', 'Female', 'Camilaca', '1998-04-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (442, '24-8572178', '004', 'Ronda', 'McAllen', 'rmcallenc8@wired.com', 'Female', 'Siquanpu', '1976-10-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (443, '07-7134798', '006', 'Demetra', 'Langhorn', 'dlanghornc9@nbcnews.com', 'Female', 'Paris 15', '1984-08-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (444, '43-6343861', '005', 'Perl', 'Quene', 'pqueneca@bandcamp.com', 'Female', 'Cilimus Kulon', '1991-09-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (445, '08-5863234', '003', 'Aileen', 'Hefforde', 'aheffordecb@weibo.com', 'Female', 'Dongchuan', '1984-12-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (446, '52-5098101', '004', 'Roman', 'Francklin', 'rfrancklincc@sbwire.com', 'Male', 'Yipeng', '1991-08-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (447, '03-1390916', '001', 'Orson', 'Lyle', 'olylecd@dagondesign.com', 'Male', 'Kishk-e Nakhūd', '1997-03-20', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (448, '59-8016316', '003', 'Glenden', 'Goulder', 'ggoulderce@istockphoto.com', 'Male', 'Cabanbanan', '1976-04-11', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (449, '30-8074237', '001', 'Daniela', 'Campanelli', 'dcampanellicf@imageshack.us', 'Female', 'Kotadukuh', '1972-06-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (450, '94-8987961', '002', 'Christen', 'Goodlatt', 'cgoodlattcg@blog.com', 'Female', 'Oepuah', '1989-04-29', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (451, '99-8014338', '007', 'Marjory', 'Magner', 'mmagnerch@cmu.edu', 'Female', 'Laojieji', '1986-10-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (452, '55-9753684', '008', 'Salaidh', 'Tregale', 'stregaleci@discovery.com', 'Female', 'Guimarães', '1981-06-23', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (453, '63-8926782', '004', 'Gael', 'Goalby', 'ggoalbycj@businessinsider.com', 'Male', 'Wysokie Mazowieckie', '1991-02-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (454, '01-1226626', '006', 'Darren', 'Keme', 'dkemeck@nationalgeographic.com', 'Male', 'Yicheng', '1984-02-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (455, '86-7441603', '001', 'Cal', 'Birrane', 'cbirranecl@skyrock.com', 'Female', 'Wushi', '1995-08-14', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (456, '21-7814731', '008', 'Sigismondo', 'Perrigo', 'sperrigocm@narod.ru', 'Male', 'Luzhou', '1995-10-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (457, '79-6251867', '006', 'Si', 'Ranvoise', 'sranvoisecn@dropbox.com', 'Male', 'Xuancheng', '1979-09-26', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (458, '24-2483174', '008', 'Pammie', 'Kiloh', 'pkilohco@artisteer.com', 'Female', 'Hanyuan', '1997-04-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (459, '12-5458967', '003', 'Carmita', 'Dunrige', 'cdunrigecp@ebay.co.uk', 'Female', 'Baoping', '1976-11-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (460, '34-8491992', '001', 'Cody', 'Laskey', 'claskeycq@gnu.org', 'Male', 'Lam Sonthi', '1983-12-29', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (461, '83-9710945', '006', 'Hamish', 'Blackader', 'hblackadercr@oracle.com', 'Male', 'Rączna', '1972-07-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (462, '06-4872736', '006', 'Matthieu', 'Pigny', 'mpignycs@google.com.hk', 'Male', 'Fukuchiyama', '1985-08-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (463, '72-7132519', '008', 'Noellyn', 'Quittonden', 'nquittondenct@nationalgeographic.com', 'Female', 'Alexandria', '1981-10-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (464, '45-6172303', '005', 'Romola', 'Muscott', 'rmuscottcu@hc360.com', 'Female', 'Sotnikovskoye', '1974-06-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (465, '91-5251299', '006', 'Fraze', 'Humber', 'fhumbercv@slideshare.net', 'Male', 'Now Zād', '1977-04-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (466, '18-4358036', '002', 'Aurelie', 'Warsop', 'awarsopcw@buzzfeed.com', 'Female', 'Al Kiswah', '1984-06-26', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (467, '32-9748255', '005', 'Filberte', 'Mardling', 'fmardlingcx@163.com', 'Male', 'Lysekil', '1977-08-16', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (468, '26-9510807', '005', 'Claudette', 'Dyton', 'cdytoncy@google.com.br', 'Female', 'Peliyagoda', '1984-10-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (469, '43-6645724', '002', 'Aguistin', 'Spofford', 'aspoffordcz@symantec.com', 'Male', 'Hongyuan', '1987-01-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (470, '44-0760492', '007', 'Dukie', 'Hotchkin', 'dhotchkind0@flavors.me', 'Male', 'Burgersdorp', '1977-11-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (471, '71-9555211', '001', 'Stevy', 'Apthorpe', 'sapthorped1@joomla.org', 'Male', 'Wanghu', '1988-12-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (472, '37-1892109', '002', 'Guglielma', 'Itzhaki', 'gitzhakid2@howstuffworks.com', 'Female', 'Pryluky', '1976-01-11', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (473, '93-3439674', '003', 'Flem', 'Monnery', 'fmonneryd3@columbia.edu', 'Male', 'Buena Vista', '1992-12-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (474, '53-5937553', '004', 'Bobina', 'Morkham', 'bmorkhamd4@over-blog.com', 'Female', 'Ban Nahin', '1975-05-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (475, '76-3057153', '003', 'Viki', 'Ewer', 'vewerd5@xrea.com', 'Female', 'Mo I Rana', '1982-02-13', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (476, '08-1368639', '003', 'Bethena', 'Indgs', 'bindgsd6@virginia.edu', 'Female', 'Acacías', '1976-08-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (477, '42-6516887', '004', 'Vale', 'Canedo', 'vcanedod7@wp.com', 'Male', 'Kangar', '1978-12-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (478, '78-7073175', '008', 'Karoly', 'Rickertsen', 'krickertsend8@adobe.com', 'Female', 'Mina Clavero', '1990-04-10', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (479, '42-0433993', '002', 'Court', 'Heersma', 'cheersmad9@utexas.edu', 'Male', 'San Jose', '1978-06-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (480, '93-8368795', '006', 'Myrle', 'Kiddie', 'mkiddieda@umich.edu', 'Female', 'Monte-Carlo', '1991-08-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (481, '47-9635019', '006', 'Salli', 'Mariotte', 'smariottedb@gov.uk', 'Female', 'Kidal', '1981-12-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (482, '58-6762520', '004', 'Guilbert', 'McDougald', 'gmcdougalddc@netscape.com', 'Male', 'Rucheng Chengguanzhen', '1993-02-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (483, '11-6362919', '008', 'Aldo', 'Gaydon', 'agaydondd@hostgator.com', 'Male', 'Vrbovec', '1982-06-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (484, '37-2459107', '005', 'Natala', 'Haig', 'nhaigde@liveinternet.ru', 'Female', 'Aseri', '1977-08-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (485, '24-8560427', '008', 'Abbe', 'Tottie', 'atottiedf@cafepress.com', 'Male', 'Pom Prap Sattru Phai', '1977-08-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (486, '48-4567864', '005', 'Avrit', 'Crayke', 'acraykedg@ezinearticles.com', 'Female', 'Gines-Patay', '1989-12-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (487, '35-1172408', '007', 'Gawen', 'Awcoate', 'gawcoatedh@tumblr.com', 'Male', 'Kalidawe', '1991-08-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (488, '17-2050745', '006', 'Rosella', 'Krol', 'rkroldi@livejournal.com', 'Female', 'Granja', '1976-06-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (489, '36-8328591', '004', 'Lea', 'Mc Queen', 'lmcqueendj@blogs.com', 'Female', 'Niugan', '1994-05-10', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (490, '74-2331226', '004', 'Granthem', 'MacPaik', 'gmacpaikdk@digg.com', 'Male', 'Bonanza', '1987-10-01', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (491, '58-3633030', '001', 'Nick', 'Kaesmans', 'nkaesmansdl@prnewswire.com', 'Male', 'Margate', '1993-06-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (492, '52-5822086', '006', 'Glendon', 'Asher', 'gasherdm@hc360.com', 'Male', 'Waekolong', '1981-08-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (493, '42-2803428', '007', 'Cos', 'Simpole', 'csimpoledn@parallels.com', 'Male', 'Pariti', '1991-06-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (494, '77-9492799', '005', 'Zea', 'Siggin', 'zsiggindo@npr.org', 'Female', 'Lidingö', '1983-04-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (495, '19-4887000', '003', 'Andee', 'Cheesworth', 'acheesworthdp@timesonline.co.uk', 'Female', 'Knin', '1974-03-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (496, '84-0587947', '005', 'Carter', 'O''Loinn', 'coloinndq@jalbum.net', 'Male', 'Nanyaojie', '1993-01-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (497, '31-5505589', '002', 'Chase', 'Tilsley', 'ctilsleydr@pbs.org', 'Male', 'Qārah', '1977-07-26', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (498, '95-3211031', '008', 'Livia', 'Gorthy', 'lgorthyds@exblog.jp', 'Female', 'Zaragoza', '1973-12-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (499, '08-9275314', '003', 'Vail', 'Schinetti', 'vschinettidt@aol.com', 'Male', 'San Pedro de Macorís', '1988-09-10', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (500, '05-8086008', '005', 'Farlay', 'Mendonca', 'fmendoncadu@shareasale.com', 'Male', 'Cibanten', '1997-08-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (501, '24-1475937', '003', 'Nev', 'Southby', 'nsouthbydv@reddit.com', 'Male', 'Gweedore', '1987-09-06', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (502, '35-9336012', '002', 'Rafaelita', 'Bowring', 'rbowringdw@reuters.com', 'Female', 'Casal', '1984-03-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (503, '85-9660500', '006', 'Dagmar', 'Mandrake', 'dmandrakedx@myspace.com', 'Female', 'Erlangkou', '1979-10-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (504, '85-3967468', '005', 'Jehu', 'Cockshott', 'jcockshottdy@friendfeed.com', 'Male', 'Bouarouss', '1979-09-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (505, '14-7031558', '006', 'Fan', 'Sherringham', 'fsherringhamdz@yale.edu', 'Female', 'Ljubešćica', '1996-04-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (506, '44-2450284', '005', 'Ringo', 'Comettoi', 'rcomettoie0@harvard.edu', 'Male', 'Benito Juarez', '1988-03-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (507, '62-8999077', '008', 'Leoline', 'Grishkov', 'lgrishkove1@samsung.com', 'Female', 'Júlio de Castilhos', '1989-08-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (508, '95-1036175', '002', 'Tatiana', 'Kingsly', 'tkingslye2@furl.net', 'Female', 'Caomiao', '1974-05-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (509, '54-1114805', '004', 'Vite', 'Dacey', 'vdaceye3@narod.ru', 'Male', 'Yi’an', '1984-08-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (510, '36-1657968', '005', 'Merralee', 'Wint', 'mwinte4@surveymonkey.com', 'Female', 'E’zhou', '1977-12-16', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (511, '16-3833200', '008', 'Jarret', 'Jelleman', 'jjellemane5@bloomberg.com', 'Male', 'Sūrak', '1974-10-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (512, '27-3604479', '004', 'Shalne', 'Sibery', 'ssiberye6@cpanel.net', 'Female', 'Ronggui', '1993-01-19', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (513, '52-0985630', '004', 'Daloris', 'O''Doherty', 'dodohertye7@arizona.edu', 'Female', 'Sumberbakti', '1993-03-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (514, '03-8322308', '003', 'Cheryl', 'Dorwood', 'cdorwoode8@hud.gov', 'Female', 'Hucheng', '1997-07-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (515, '71-7845471', '002', 'Harmon', 'Brunt', 'hbrunte9@cdc.gov', 'Male', 'Ojo de Agua', '1976-11-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (516, '81-6967299', '007', 'Trula', 'Lourenco', 'tlourencoea@google.co.uk', 'Female', 'Ulluaya', '1984-11-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (517, '48-8351540', '002', 'Freeman', 'Holah', 'fholaheb@adobe.com', 'Male', 'Ar Rifā‘', '1980-11-10', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (518, '69-6030017', '001', 'Roger', 'Frissell', 'rfrissellec@sphinn.com', 'Male', 'Tánh Linh', '1976-07-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (519, '90-0157369', '003', 'Pet', 'Bull', 'pbulled@themeforest.net', 'Female', 'Mó', '1986-08-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (520, '34-7533897', '006', 'Maia', 'Houtby', 'mhoutbyee@flickr.com', 'Female', 'Ambunti', '1985-01-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (521, '13-2075193', '004', 'Randolph', 'Kovalski', 'rkovalskief@123-reg.co.uk', 'Male', 'Huangfang', '1972-07-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (522, '80-9312268', '004', 'Anatollo', 'Pheby', 'aphebyeg@answers.com', 'Male', 'Ad Dīs ash Sharqīyah', '1996-09-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (523, '73-6349483', '008', 'Darrell', 'MacInherney', 'dmacinherneyeh@example.com', 'Male', 'Lafia', '1977-09-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (524, '52-2410264', '005', 'Gay', 'Celli', 'gcelliei@weibo.com', 'Female', 'Prupuh', '1977-04-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (525, '86-1744492', '006', 'Emmeline', 'Pisculli', 'episculliej@japanpost.jp', 'Female', 'Péfki', '1988-09-11', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (526, '13-7952308', '005', 'Regan', 'Penwright', 'rpenwrightek@nifty.com', 'Female', 'Labansari', '1988-07-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (527, '20-6606421', '001', 'Pavel', 'Biggin', 'pbigginel@cdbaby.com', 'Male', 'Canaman', '1971-07-05', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (528, '40-2311344', '006', 'Sol', 'Brandone', 'sbrandoneem@over-blog.com', 'Male', 'Taiping', '1975-09-05', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (529, '78-0402247', '008', 'Aubree', 'Burburough', 'aburburoughen@dailymail.co.uk', 'Female', 'Wiset Chaichan', '1975-02-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (530, '10-0486682', '001', 'Scarlet', 'Molan', 'smolaneo@rambler.ru', 'Female', 'Bibrka', '1973-07-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (531, '45-8343644', '006', 'Kerrie', 'Casaroli', 'kcasaroliep@bing.com', 'Female', 'Pingfeng', '1982-12-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (532, '97-6015776', '003', 'Clo', 'Vickerman', 'cvickermaneq@imageshack.us', 'Female', 'Putun', '1985-03-31', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (533, '19-8237084', '002', 'Ulysses', 'McGeever', 'umcgeeverer@dropbox.com', 'Male', 'Kasugai', '1984-01-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (534, '86-3501484', '001', 'Coop', 'Guilder', 'cguilderes@altervista.org', 'Male', 'Yantai', '1979-10-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (535, '43-8927754', '005', 'Cory', 'Dell Casa', 'cdellcasaet@1und1.de', 'Female', 'Limen', '1985-03-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (536, '99-8458821', '002', 'Lisetta', 'Gooly', 'lgoolyeu@gov.uk', 'Female', 'Chashnikovo', '1994-07-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (537, '47-9943345', '003', 'Marylinda', 'Chue', 'mchueev@google.es', 'Female', 'Atamyrat', '1976-09-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (538, '50-8715302', '001', 'Rea', 'Goodday', 'rgooddayew@mlb.com', 'Female', 'Wenfu', '1997-04-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (539, '29-0119116', '007', 'Virgie', 'Rabb', 'vrabbex@dyndns.org', 'Male', 'Qiawan', '1976-08-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (540, '37-1885939', '001', 'Tamma', 'Tolwood', 'ttolwoodey@miitbeian.gov.cn', 'Female', 'Anticala', '1992-10-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (541, '51-1669001', '005', 'Brandon', 'Vickress', 'bvickressez@virginia.edu', 'Male', 'Tarrafal de São Nicolau', '1984-12-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (542, '46-7732227', '004', 'Kore', 'Stillman', 'kstillmanf0@baidu.com', 'Female', 'Manevychi', '1975-05-25', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (543, '68-7817055', '008', 'Giana', 'Bengtson', 'gbengtsonf1@tamu.edu', 'Female', 'Dakhla', '1990-03-09', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (544, '85-5920854', '008', 'Gabbie', 'Androletti', 'gandrolettif2@mayoclinic.com', 'Female', 'Promissão', '1979-12-30', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (545, '27-9992850', '005', 'Domini', 'Howgate', 'dhowgatef3@baidu.com', 'Female', 'Firá', '1976-09-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (546, '70-6426135', '003', 'Alexis', 'Rief', 'arieff4@macromedia.com', 'Female', 'Kiloloran', '1972-08-16', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (547, '48-4082063', '008', 'Bobbette', 'Drayton', 'bdraytonf5@phpbb.com', 'Female', 'Skaramangás', '1973-06-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (548, '63-3476406', '001', 'Bale', 'Emmatt', 'bemmattf6@mapquest.com', 'Male', 'Bodø', '1988-01-04', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (549, '01-8196537', '003', 'Jo ann', 'Pariso', 'jparisof7@spiegel.de', 'Female', 'Cibeureum', '1983-10-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (550, '73-5001329', '007', 'Garth', 'Chawner', 'gchawnerf8@edublogs.org', 'Male', 'Komiža', '1988-01-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (551, '52-7823692', '001', 'Jolie', 'Ratke', 'jratkef9@fotki.com', 'Female', 'Dongxiang', '1994-05-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (552, '33-4908789', '008', 'Roger', 'Bloys', 'rbloysfa@angelfire.com', 'Male', 'Bautista', '1979-02-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (553, '50-7266839', '004', 'Devlin', 'Oxterby', 'doxterbyfb@weibo.com', 'Male', 'Karakulino', '1972-04-13', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (554, '34-6303505', '002', 'Connie', 'Pley', 'cpleyfc@ycombinator.com', 'Male', 'Xiaba', '1973-01-16', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (555, '80-6420035', '006', 'Lester', 'Lidgard', 'llidgardfd@psu.edu', 'Male', 'Hengbanqiao', '1991-05-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (556, '31-1319365', '001', 'Abbey', 'Fetherstonhaugh', 'afetherstonhaughfe@addthis.com', 'Female', 'Sicaya', '1984-02-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (557, '11-2356521', '002', 'Rosabella', 'Caulkett', 'rcaulkettff@lycos.com', 'Female', 'Elvas', '1994-06-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (558, '72-3440819', '002', 'Carmela', 'Vellacott', 'cvellacottfg@tmall.com', 'Female', 'Jardín', '1978-11-12', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (559, '46-6808329', '007', 'Bel', 'Carey', 'bcareyfh@g.co', 'Female', 'Aryiropoúlion', '1996-12-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (560, '92-3154242', '008', 'Teodoro', 'Ginnelly', 'tginnellyfi@wikispaces.com', 'Male', 'Vũ Quang', '1990-12-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (561, '14-6901239', '004', 'Staci', 'Dawdry', 'sdawdryfj@nationalgeographic.com', 'Female', 'Takedamachi', '1986-05-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (562, '36-1794891', '006', 'Babbette', 'Tinto', 'btintofk@utexas.edu', 'Female', 'Nikolayevskaya', '1980-12-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (563, '11-7205971', '004', 'Roze', 'Upsale', 'rupsalefl@mashable.com', 'Female', 'Chahe', '1990-11-07', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (564, '64-8747054', '003', 'Ines', 'Gavrieli', 'igavrielifm@cam.ac.uk', 'Female', 'Przyborów', '1990-04-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (565, '80-1051232', '002', 'Sibbie', 'Rosendall', 'srosendallfn@fc2.com', 'Female', 'Tapas', '1974-11-24', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (566, '06-0164936', '007', 'Joaquin', 'Winkle', 'jwinklefo@bloglovin.com', 'Male', 'Mondlo', '1983-12-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (567, '12-4676333', '007', 'Meggi', 'Titlow', 'mtitlowfp@zdnet.com', 'Female', 'Gómfoi', '1980-02-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (568, '12-4856797', '001', 'Ario', 'Baish', 'abaishfq@uiuc.edu', 'Male', 'Jand', '1996-11-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (569, '66-0131973', '007', 'Moishe', 'Erbain', 'merbainfr@nps.gov', 'Male', 'Ōtawara', '1987-01-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (570, '80-6460863', '008', 'Jacquetta', 'Phalp', 'jphalpfs@google.com', 'Female', 'Valparaíso', '1989-09-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (571, '67-1221573', '008', 'Kristal', 'Petraitis', 'kpetraitisft@google.fr', 'Female', 'Castelnaudary', '1995-03-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (572, '25-9007869', '007', 'Hieronymus', 'Makepeace', 'hmakepeacefu@bloglines.com', 'Male', 'Pingpo', '1983-01-13', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (573, '32-5384066', '002', 'Stanislas', 'Dear', 'sdearfv@goodreads.com', 'Male', 'Bohai', '1987-05-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (574, '07-8514442', '001', 'Teirtza', 'McDonald', 'tmcdonaldfw@sohu.com', 'Female', 'Landi Kotal', '1971-12-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (575, '74-4436246', '005', 'Iorgos', 'Bakesef', 'ibakeseffx@cdbaby.com', 'Male', 'Krzyżanów', '1993-01-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (576, '74-7947440', '003', 'Rosita', 'Plaster', 'rplasterfy@friendfeed.com', 'Female', 'Qingxi', '1973-04-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (577, '15-9947516', '001', 'Vail', 'Marklow', 'vmarklowfz@constantcontact.com', 'Male', 'Tseel', '1993-05-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (578, '08-0207374', '008', 'Kaleena', 'Melbert', 'kmelbertg0@aol.com', 'Female', 'Nueve de Julio', '1983-03-20', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (579, '22-5794958', '008', 'Teodor', 'Ollerhead', 'tollerheadg1@dion.ne.jp', 'Male', 'Traiguén', '1972-02-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (580, '66-4027457', '005', 'Tabatha', 'Huckle', 'thuckleg2@tiny.cc', 'Female', 'Bengubelan', '1995-04-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (581, '91-5501368', '002', 'Harlie', 'Gerrett', 'hgerrettg3@patch.com', 'Female', 'San Esteban', '1987-08-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (582, '52-5136409', '005', 'Rosalinde', 'Shorthill', 'rshorthillg4@kickstarter.com', 'Female', 'Xueshan', '1980-07-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (583, '68-0384588', '004', 'Erie', 'Sinclaire', 'esinclaireg5@ovh.net', 'Male', 'La Celia', '1984-06-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (584, '42-4709830', '007', 'Sarina', 'Baswall', 'sbaswallg6@stanford.edu', 'Female', 'Jarash', '1976-12-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (585, '74-9590743', '004', 'Selie', 'Andersson', 'sanderssong7@xinhuanet.com', 'Female', 'Vällingby', '1985-04-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (586, '79-0674758', '002', 'Steffen', 'Nelius', 'sneliusg8@nydailynews.com', 'Male', 'Aguiar da Beira', '1977-09-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (587, '34-8324116', '004', 'Donnie', 'Torbeck', 'dtorbeckg9@rediff.com', 'Male', 'Bununu Kasa', '1990-11-02', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (588, '36-6934523', '005', 'Isidore', 'Cotty', 'icottyga@twitpic.com', 'Male', 'Tanabe', '1972-01-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (589, '15-9122610', '008', 'Fayina', 'Ramsbotham', 'framsbothamgb@goo.gl', 'Female', 'Tiébo', '1974-07-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (590, '16-7699066', '008', 'Jenelle', 'Maggs', 'jmaggsgc@icio.us', 'Female', 'Bessonovka', '1983-03-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (591, '53-5078780', '007', 'Agna', 'Billings', 'abillingsgd@purevolume.com', 'Female', 'Milot', '1982-01-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (592, '08-7088350', '008', 'Brinn', 'Saladin', 'bsaladinge@abc.net.au', 'Female', 'Mouhijärvi', '1990-04-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (593, '14-5701276', '004', 'Berty', 'Copeman', 'bcopemangf@gov.uk', 'Male', 'Kadoma', '1996-09-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (594, '17-2358009', '006', 'Fania', 'Heaseman', 'fheasemangg@hexun.com', 'Female', 'Stepojevac', '1976-02-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (595, '68-5970342', '006', 'Ingram', 'Chippin', 'ichippingh@sakura.ne.jp', 'Male', 'Dahe', '1991-11-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (596, '93-3015587', '002', 'Muhammad', 'Gossington', 'mgossingtongi@posterous.com', 'Male', 'Utama Wetan', '1997-08-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (597, '94-9316641', '005', 'Gayler', 'Blincko', 'gblinckogj@hubpages.com', 'Male', 'Sobral da Abelheira', '1971-11-01', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (598, '92-0298893', '006', 'Justino', 'Organ', 'jorgangk@ox.ac.uk', 'Male', 'Stockholm', '1975-06-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (599, '24-8448788', '004', 'Diena', 'McGibbon', 'dmcgibbongl@addthis.com', 'Female', 'Banjeru', '1973-11-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (600, '98-0916520', '004', 'Arlyn', 'Vasilyevski', 'avasilyevskigm@bloglines.com', 'Female', 'Tsubata', '1981-11-30', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (601, '40-6688140', '003', 'Rebbecca', 'Philp', 'rphilpgn@moonfruit.com', 'Female', 'Shuanghe', '1989-01-11', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (602, '44-4906505', '005', 'Padget', 'Fetherston', 'pfetherstongo@xrea.com', 'Male', 'Banpu', '1975-06-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (603, '81-5491396', '004', 'Karry', 'Knapper', 'kknappergp@businessweek.com', 'Female', 'Cergy-Pontoise', '1986-10-01', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (604, '20-8142911', '001', 'Rupert', 'Harcombe', 'rharcombegq@amazon.co.jp', 'Male', 'Saint-Herblain', '1990-08-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (605, '10-9230767', '004', 'Giavani', 'Lawrie', 'glawriegr@wikispaces.com', 'Male', 'Hilversum', '1973-11-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (606, '98-3932038', '003', 'Jilli', 'Vasentsov', 'jvasentsovgs@deviantart.com', 'Female', 'Mataguži', '1983-02-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (607, '03-5343348', '004', 'Sabina', 'MacConnechie', 'smacconnechiegt@hud.gov', 'Female', 'Mananum', '1983-04-05', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (608, '27-7319371', '005', 'Kaycee', 'Tremethack', 'ktremethackgu@unc.edu', 'Female', 'Xiejia', '1984-08-10', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (609, '28-4088734', '004', 'Keir', 'Aicken', 'kaickengv@hao123.com', 'Male', 'Baoluan', '1995-11-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (610, '04-4430307', '005', 'Constantina', 'Kiwitz', 'ckiwitzgw@google.ru', 'Female', 'Wudangshan', '1990-07-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (611, '82-6128426', '005', 'Tova', 'Draisey', 'tdraiseygx@blogger.com', 'Female', 'Nidri', '1988-07-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (612, '30-0233118', '007', 'Christian', 'Goeff', 'cgoeffgy@marriott.com', 'Male', 'Zemun', '1971-07-07', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (613, '39-4152112', '003', 'Alis', 'Poulsom', 'apoulsomgz@ow.ly', 'Female', 'Sumberjeruk', '1982-02-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (614, '77-8982931', '008', 'Clarabelle', 'Manilo', 'cmaniloh0@sphinn.com', 'Female', 'Maintang', '1996-03-01', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (615, '94-0489456', '005', 'Ranice', 'Coldtart', 'rcoldtarth1@flickr.com', 'Female', 'Rozwadza', '1998-02-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (616, '85-9229206', '007', 'Natalya', 'Keyhoe', 'nkeyhoeh2@wunderground.com', 'Female', 'Rehnān', '1971-09-18', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (617, '58-1726044', '005', 'Junette', 'Bainbridge', 'jbainbridgeh3@epa.gov', 'Female', 'Uubulan', '1975-05-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (618, '81-7340622', '008', 'Laureen', 'Medforth', 'lmedforthh4@bbb.org', 'Female', 'Buliran', '1972-02-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (619, '29-0710325', '001', 'Gino', 'Cattermole', 'gcattermoleh5@yellowpages.com', 'Male', 'Krasnogorskiy', '1977-01-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (620, '47-8006321', '004', 'Karel', 'Andreasen', 'kandreasenh6@sina.com.cn', 'Female', 'Guam Government House', '1974-06-20', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (621, '46-1635843', '002', 'Ossie', 'Derx', 'oderxh7@facebook.com', 'Male', 'Ávato', '1991-10-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (622, '61-0567559', '002', 'Bruno', 'Vasyatkin', 'bvasyatkinh8@biblegateway.com', 'Male', 'Shambu', '1980-07-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (623, '08-0562413', '003', 'Poppy', 'Jumont', 'pjumonth9@ft.com', 'Female', 'Morfovoúni', '1987-02-14', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (624, '50-3890167', '006', 'Gram', 'Youson', 'gyousonha@issuu.com', 'Male', 'Manzanares', '1977-06-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (625, '70-3684967', '005', 'Claus', 'Ciccottini', 'cciccottinihb@miibeian.gov.cn', 'Male', 'Slovenska Bistrica', '1975-06-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (626, '84-0208267', '008', 'Any', 'Whytock', 'awhytockhc@51.la', 'Male', 'Gerdu', '1985-03-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (627, '16-2759511', '006', 'Florentia', 'Cotterill', 'fcotterillhd@washingtonpost.com', 'Female', 'Manggekompo', '1982-12-11', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (628, '13-6379583', '008', 'Indira', 'Pettit', 'ipettithe@who.int', 'Female', 'Baiju', '1981-02-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (629, '79-7794043', '007', 'Ajay', 'Sherratt', 'asherratthf@google.es', 'Female', 'Sundsvall', '1982-01-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (630, '32-7007102', '006', 'Luciano', 'Fremantle', 'lfremantlehg@so-net.ne.jp', 'Male', 'Tongyuanpu', '1987-02-27', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (631, '89-1772322', '006', 'Shirl', 'Spadelli', 'sspadellihh@icio.us', 'Female', 'Looc', '1983-06-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (632, '65-4702877', '006', 'Carver', 'Mc Combe', 'cmccombehi@google.ca', 'Male', 'Fuling', '1988-05-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (633, '45-8911009', '006', 'Joly', 'Wroath', 'jwroathhj@merriam-webster.com', 'Female', 'San José de Feliciano', '1992-11-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (634, '54-7207885', '005', 'Merwyn', 'Upstell', 'mupstellhk@so-net.ne.jp', 'Male', 'El Limon', '1977-12-04', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (635, '52-0929584', '002', 'Lyn', 'Guyon', 'lguyonhl@mozilla.org', 'Female', 'Livádion', '1984-01-20', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (636, '94-0433189', '001', 'Philippe', 'Grimston', 'pgrimstonhm@google.cn', 'Female', 'Osilnica', '1984-01-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (637, '22-8483724', '003', 'Gerrie', 'Surmeyer', 'gsurmeyerhn@ning.com', 'Male', 'Santiago', '1973-03-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (638, '60-8921615', '007', 'Cristina', 'Baroche', 'cbarocheho@lulu.com', 'Female', 'Lisewo Malborskie', '1990-07-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (639, '81-1350240', '006', 'Andrew', 'Renhard', 'arenhardhp@taobao.com', 'Male', 'Longhua', '1987-03-21', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (640, '09-5494920', '003', 'Jeno', 'Krikorian', 'jkrikorianhq@canalblog.com', 'Male', 'Debre Sīna', '1987-01-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (641, '00-5193665', '003', 'Ermengarde', 'Wince', 'ewincehr@washingtonpost.com', 'Female', 'Manique', '1988-07-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (642, '70-0274064', '008', 'Gerianna', 'Gianulli', 'ggianullihs@technorati.com', 'Female', 'Lavras', '1984-06-15', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (643, '34-9874476', '008', 'Babs', 'Jaram', 'bjaramht@infoseek.co.jp', 'Female', 'Jishan', '1980-04-22', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (644, '42-5393640', '003', 'Brennan', 'Renols', 'brenolshu@unesco.org', 'Male', 'Golopau', '1993-05-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (645, '66-5812555', '002', 'Theodore', 'Aslott', 'taslotthv@nih.gov', 'Male', 'Wuning', '1973-09-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (646, '67-6410428', '004', 'Sigismond', 'Chasmer', 'schasmerhw@clickbank.net', 'Male', 'Jincheng', '1973-01-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (647, '92-7419780', '004', 'Allison', 'Rahl', 'arahlhx@businesswire.com', 'Female', 'Huamachuco', '1973-09-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (648, '96-2802678', '006', 'Bendix', 'McAllan', 'bmcallanhy@storify.com', 'Male', 'Zhemtala', '1988-11-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (649, '23-9242623', '002', 'Tim', 'Klamman', 'tklammanhz@reference.com', 'Male', 'Yuen Long Kau Hui', '1986-02-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (650, '48-3661745', '003', 'Lorenza', 'Eustace', 'leustacei0@sakura.ne.jp', 'Female', 'Phayuha Khiri', '1980-03-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (651, '97-8503047', '005', 'Rolland', 'Kenrack', 'rkenracki1@paginegialle.it', 'Male', 'Yangchun', '1992-01-10', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (652, '23-4233613', '008', 'Abba', 'Duinbleton', 'aduinbletoni2@thetimes.co.uk', 'Male', 'Flying Fish Cove', '1981-11-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (653, '23-3810258', '001', 'Way', 'Linn', 'wlinni3@miibeian.gov.cn', 'Male', 'Zhengcun', '1976-11-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (654, '74-7328583', '005', 'Aldridge', 'Impey', 'aimpeyi4@phoca.cz', 'Male', 'Zhifang', '1979-02-01', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (655, '87-3421231', '005', 'Babbie', 'Safont', 'bsafonti5@storify.com', 'Female', 'San José de Cusmapa', '1980-06-08', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (656, '82-8253111', '003', 'Leta', 'Broadfoot', 'lbroadfooti6@shop-pro.jp', 'Female', 'Vichuga', '1982-12-23', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (657, '06-6805374', '007', 'Bond', 'Worster', 'bworsteri7@hhs.gov', 'Male', 'Nanganumba', '1985-05-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (658, '72-2696993', '001', 'Davie', 'Crommett', 'dcrommetti8@sfgate.com', 'Male', 'Dasongshu', '1984-04-07', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (659, '61-5948321', '004', 'Linet', 'White', 'lwhitei9@independent.co.uk', 'Female', 'Ampelókipoi', '1993-12-06', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (660, '47-7945757', '006', 'Caryn', 'Gashion', 'cgashionia@discuz.net', 'Female', 'Alhambra', '1976-08-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (661, '55-4594238', '003', 'Sloane', 'Krale', 'skraleib@sogou.com', 'Male', 'Cergy-Pontoise', '1972-06-02', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (662, '00-9763367', '006', 'Shaw', 'Heinssen', 'sheinssenic@walmart.com', 'Male', 'Garden Grove', '1982-10-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (663, '03-1392953', '006', 'Ives', 'Tucsell', 'itucsellid@ucla.edu', 'Male', 'Santiago dos Velhos', '1988-05-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (664, '05-6933205', '008', 'Minni', 'Giraudo', 'mgiraudoie@nyu.edu', 'Female', 'Xiangtan', '1986-04-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (665, '69-0403516', '001', 'Tyrone', 'Axup', 'taxupif@google.com.au', 'Male', 'Risaralda', '1994-04-03', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (666, '87-7746293', '005', 'Rusty', 'Bolus', 'rbolusig@e-recht24.de', 'Male', 'Montluçon', '1974-12-19', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (667, '66-1877967', '007', 'Zandra', 'Quinane', 'zquinaneih@nsw.gov.au', 'Female', 'Winton', '1985-02-18', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (668, '12-0162657', '007', 'Orella', 'Bukac', 'obukacii@forbes.com', 'Female', 'Nynäshamn', '1991-09-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (669, '72-9199108', '005', 'Cullan', 'Rawe', 'craweij@fc2.com', 'Male', 'Wufeng', '1991-01-11', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (670, '34-3871279', '008', 'Dayna', 'Doncaster', 'ddoncasterik@deviantart.com', 'Female', 'Hihyā', '1972-05-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (671, '68-7934572', '007', 'Alexei', 'Leathard', 'aleathardil@wikipedia.org', 'Male', 'Kiuola', '1989-08-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (672, '75-4929880', '001', 'Alexi', 'Smee', 'asmeeim@ocn.ne.jp', 'Female', 'Reguengos de Monsaraz', '1990-12-22', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (673, '22-5105461', '001', 'Curtice', 'Broader', 'cbroaderin@google.com.hk', 'Male', 'Przemków', '1971-09-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (674, '38-4198072', '004', 'Malva', 'Swafford', 'mswaffordio@ask.com', 'Female', 'Monastyrshchina', '1996-09-24', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (675, '69-2835923', '003', 'Annabella', 'Barbrook', 'abarbrookip@google.it', 'Female', 'Nandasmo', '1984-04-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (676, '12-4476799', '006', 'Colan', 'Knuckles', 'cknucklesiq@pcworld.com', 'Male', 'Kutamangu', '1980-07-10', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (677, '28-7315939', '004', 'Rurik', 'Epton', 'reptonir@disqus.com', 'Male', 'Shunde', '1976-05-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (678, '30-0173078', '004', 'Temple', 'Rosa', 'trosais@themeforest.net', 'Male', 'Tianfu', '1976-06-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (679, '56-7223136', '005', 'Jens', 'Crackel', 'jcrackelit@studiopress.com', 'Male', 'Tangchi', '1993-06-15', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (680, '59-1218096', '007', 'Paulita', 'Wardale', 'pwardaleiu@examiner.com', 'Female', 'Schirmeck', '1995-06-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (681, '99-4183315', '005', 'Huntley', 'Greedyer', 'hgreedyeriv@fema.gov', 'Male', 'Nikhom Kham Soi', '1975-10-16', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (682, '24-5276668', '008', 'Floria', 'Solomonides', 'fsolomonidesiw@friendfeed.com', 'Female', 'Mateus Leme', '1986-10-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (683, '49-5549307', '005', 'Loren', 'Cicchelli', 'lcicchelliix@harvard.edu', 'Male', 'Sammatti', '1977-01-29', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (684, '43-2315779', '002', 'Erroll', 'Dibben', 'edibbeniy@webs.com', 'Male', 'Maojiagang', '1977-10-04', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (685, '26-4931638', '007', 'Ursula', 'Probyn', 'uprobyniz@tinypic.com', 'Female', 'Võhma', '1991-06-19', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (686, '48-2149191', '008', 'Cassandry', 'Orto', 'cortoj0@joomla.org', 'Female', 'Leitões', '1989-01-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (687, '65-1894858', '005', 'Derrek', 'Pitkin', 'dpitkinj1@europa.eu', 'Male', 'Wuma', '1993-10-14', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (688, '84-1690417', '006', 'Joseito', 'Naile', 'jnailej2@umich.edu', 'Male', 'Newton', '1995-10-22', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (689, '76-9639797', '001', 'Sayre', 'Del Monte', 'sdelmontej3@usatoday.com', 'Female', 'Kon Tum', '1986-12-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (690, '13-3707632', '008', 'Joachim', 'Grote', 'jgrotej4@pen.io', 'Male', 'Monticello', '1972-04-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (691, '72-9926434', '004', 'Libbey', 'Fihelly', 'lfihellyj5@weebly.com', 'Female', 'Taha Man Zu', '1991-03-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (692, '66-0427458', '006', 'Arlen', 'Jerzak', 'ajerzakj6@behance.net', 'Male', 'Huzhou', '1985-01-27', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (693, '61-9638286', '008', 'Hart', 'Teideman', 'hteidemanj7@behance.net', 'Male', 'Brest', '1985-11-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (694, '70-1642754', '002', 'Dorene', 'Jeyness', 'djeynessj8@wordpress.org', 'Female', 'Zhongshanmen', '1990-09-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (695, '96-6922631', '005', 'Gustavo', 'Smithson', 'gsmithsonj9@deviantart.com', 'Male', 'Fundación', '1994-06-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (696, '33-8855373', '006', 'Anni', 'Wardel', 'awardelja@fda.gov', 'Female', 'Zhongwei', '1989-09-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (697, '99-8833040', '002', 'Boniface', 'Richt', 'brichtjb@baidu.com', 'Male', 'Wumahe', '1995-12-22', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (698, '51-1089062', '003', 'Stacie', 'Eames', 'seamesjc@prnewswire.com', 'Female', 'Huangzhai', '1982-12-05', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (699, '80-9324916', '002', 'Gilberto', 'Aimeric', 'gaimericjd@walmart.com', 'Male', 'Vannes', '1995-12-05', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (700, '95-7617062', '004', 'Alexis', 'Buzzing', 'abuzzingje@hc360.com', 'Male', 'Taquile', '1993-11-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (701, '03-3997917', '008', 'Giralda', 'Pocknoll', 'gpocknolljf@ted.com', 'Female', 'Yushu', '1993-03-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (702, '49-1014329', '008', 'Amandi', 'Handscombe', 'ahandscombejg@bravesites.com', 'Female', 'Jingling', '1971-12-03', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (703, '37-5165047', '007', 'Blythe', 'Noquet', 'bnoquetjh@ning.com', 'Female', 'Alor Setar', '1971-05-13', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (704, '15-2404673', '002', 'Sheri', 'O''Tuohy', 'sotuohyji@theglobeandmail.com', 'Female', 'Keruguya', '1993-12-22', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (705, '48-2050209', '004', 'Mort', 'Hilling', 'mhillingjj@twitpic.com', 'Male', 'Dikwa', '1979-05-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (706, '59-9689916', '002', 'Randal', 'Caney', 'rcaneyjk@kickstarter.com', 'Male', 'Žiželice', '1990-03-15', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (707, '90-6306885', '004', 'Renata', 'Lage', 'rlagejl@netlog.com', 'Female', 'Banepa', '1981-09-06', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (708, '96-6146260', '005', 'Adela', 'McCloughlin', 'amccloughlinjm@trellian.com', 'Female', 'Montpellier', '1982-02-11', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (709, '56-2901038', '005', 'Anthea', 'Brend', 'abrendjn@columbia.edu', 'Female', 'Xiekou', '1995-05-02', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (710, '17-0312735', '003', 'Demott', 'Halladay', 'dhalladayjo@buzzfeed.com', 'Male', 'Madrid', '1991-04-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (711, '56-4267871', '006', 'Dominga', 'Charlet', 'dcharletjp@wufoo.com', 'Female', 'Elefsína', '1978-04-21', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (712, '36-7416825', '006', 'Gerrie', 'Bushby', 'gbushbyjq@flavors.me', 'Male', 'Şafāshahr', '1978-09-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (713, '44-0211505', '008', 'Lyndsay', 'Timpany', 'ltimpanyjr@google.co.uk', 'Female', 'Gierałtowice', '1997-11-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (714, '25-4014831', '004', 'Christel', 'Spight', 'cspightjs@ibm.com', 'Female', 'Buenavista', '1990-08-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (715, '08-3228393', '005', 'Sallyanne', 'Golthorpp', 'sgolthorppjt@bbb.org', 'Female', 'Gaoyan', '1987-11-14', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (716, '72-6090824', '007', 'Adan', 'Hullock', 'ahullockju@soup.io', 'Male', 'Uddevalla', '1988-02-17', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (717, '91-7154099', '004', 'Oberon', 'Lucock', 'olucockjv@theatlantic.com', 'Male', 'Venda do Alcaide', '1977-01-25', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (718, '33-9843853', '008', 'York', 'Howick', 'yhowickjw@ftc.gov', 'Male', 'Paamiut', '1981-12-27', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (719, '02-3739447', '003', 'Hi', 'Nisco', 'hniscojx@amazonaws.com', 'Male', 'Hanam', '1975-06-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (720, '05-3477866', '002', 'Aylmer', 'Pfertner', 'apfertnerjy@slideshare.net', 'Male', 'Viale', '1989-09-28', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (721, '99-1434299', '001', 'Leonelle', 'Bootes', 'lbootesjz@army.mil', 'Female', 'Khon Kaen', '1977-04-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (722, '02-2608726', '006', 'Homere', 'Goodisson', 'hgoodissonk0@4shared.com', 'Male', 'Huikou', '1973-08-09', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (723, '33-0295970', '006', 'Mirna', 'Sindell', 'msindellk1@hao123.com', 'Female', 'Santa Cruz', '1985-07-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (724, '25-3408181', '004', 'Cleon', 'Wisson', 'cwissonk2@princeton.edu', 'Male', 'Fuzhiping', '1972-10-12', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (725, '18-0455354', '001', 'Tammy', 'Goodhew', 'tgoodhewk3@ftc.gov', 'Male', 'Milano', '1973-01-20', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (726, '87-4056015', '002', 'Nathanial', 'Jirak', 'njirakk4@samsung.com', 'Male', 'Brnaze', '1978-01-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (727, '00-6023594', '008', 'Nolana', 'Kepe', 'nkepek5@163.com', 'Female', 'Barras', '1983-12-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (728, '04-1581839', '007', 'Sheffield', 'Betty', 'sbettyk6@artisteer.com', 'Male', 'Hayes', '1979-04-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (729, '34-3011551', '002', 'Finlay', 'Cookson', 'fcooksonk7@virginia.edu', 'Male', 'Xingzhen', '1976-09-21', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (730, '10-9216156', '004', 'Eugenio', 'Gludor', 'egludork8@webnode.com', 'Male', 'Bang Bo', '1998-02-07', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (731, '88-6914446', '004', 'Aloin', 'Simioli', 'asimiolik9@hc360.com', 'Male', 'Biryulëvo', '1995-11-20', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (732, '17-8230151', '005', 'Andrey', 'Mularkey', 'amularkeyka@xing.com', 'Male', 'Halayhayin', '1984-05-10', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (733, '70-5360916', '002', 'Maribelle', 'Testro', 'mtestrokb@europa.eu', 'Female', 'Elaiochóri', '1984-08-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (734, '38-1435134', '003', 'Arlee', 'Marrow', 'amarrowkc@deviantart.com', 'Female', 'Kham Sakae Saeng', '1993-06-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (735, '25-8194367', '008', 'Nessi', 'McKague', 'nmckaguekd@dot.gov', 'Female', 'Szydłowo', '1972-03-21', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (736, '64-5871676', '008', 'Burgess', 'Burtt', 'bburttke@gov.uk', 'Male', 'Rušanj', '1991-03-04', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (737, '95-2615042', '003', 'Derrick', 'Heliet', 'dhelietkf@freewebs.com', 'Male', 'Songkhla', '1971-11-20', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (738, '66-2891491', '002', 'Prescott', 'Everix', 'peverixkg@biblegateway.com', 'Male', 'Kairouan', '1973-09-23', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (739, '67-9956830', '007', 'Jaquelin', 'Lafrentz', 'jlafrentzkh@sciencedirect.com', 'Female', 'Itabuna', '1981-07-24', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (740, '63-2849007', '005', 'Oby', 'Lorens', 'olorenski@liveinternet.ru', 'Male', 'Banjar Geriana Kangin', '1990-05-01', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (741, '60-7031799', '005', 'Stearn', 'Quade', 'squadekj@irs.gov', 'Male', 'Zuocun', '1986-12-26', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (742, '20-4095890', '001', 'Brandyn', 'Campana', 'bcampanakk@amazon.de', 'Male', 'Shah Alam', '1977-01-15', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (743, '79-4549523', '006', 'Neill', 'Smewings', 'nsmewingskl@dell.com', 'Male', 'Chiang Mai', '1973-02-25', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (744, '12-9837910', '006', 'Ravi', 'Kynnd', 'rkynndkm@dot.gov', 'Male', 'El Fasher', '1986-02-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (745, '34-8959662', '008', 'Sean', 'Tenbrug', 'stenbrugkn@state.tx.us', 'Female', 'Aviá Terai', '1979-04-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (746, '43-7843409', '003', 'Alden', 'Coffey', 'acoffeyko@gravatar.com', 'Male', 'Yinjiang', '1973-09-27', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (747, '66-3431124', '008', 'Nanine', 'Stroban', 'nstrobankp@squarespace.com', 'Female', 'Kolomanu', '1996-11-28', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (748, '02-7981870', '001', 'Delcina', 'Depper', 'ddepperkq@opensource.org', 'Female', 'Sekararum', '1984-05-30', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (749, '04-2380838', '004', 'Filmore', 'Hambridge', 'fhambridgekr@go.com', 'Male', 'Tarui', '1978-07-08', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (750, '67-4736986', '008', 'Savina', 'Sandall', 'ssandallks@newsvine.com', 'Female', 'Sao Hai', '1983-06-10', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (751, '12-3317803', '003', 'Kellby', 'Alessandretti', 'kalessandrettikt@salon.com', 'Male', 'Radostowice', '1981-10-17', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (752, '02-2409406', '004', 'Derry', 'Beedle', 'dbeedleku@last.fm', 'Male', 'Zastron', '1974-05-14', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (753, '14-6102477', '004', 'Brewer', 'Skewes', 'bskeweskv@buzzfeed.com', 'Male', 'Kari', '1980-11-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (754, '38-5812474', '006', 'Georgianna', 'Breache', 'gbreachekw@gmpg.org', 'Female', 'Cambebba', '1995-09-15', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (755, '90-0914615', '001', 'Grenville', 'Avrahamoff', 'gavrahamoffkx@about.com', 'Male', 'Alcala', '1985-06-12', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (756, '79-1445754', '002', 'Mandy', 'Denzey', 'mdenzeyky@npr.org', 'Female', 'Long Beach', '1974-01-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (757, '61-0476580', '006', 'Giffie', 'Follacaro', 'gfollacarokz@discuz.net', 'Male', 'Drezdenko', '1989-12-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (758, '27-1631262', '006', 'Claribel', 'Giacomoni', 'cgiacomonil0@multiply.com', 'Female', 'Cimongkor', '1981-08-18', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (759, '84-9590919', '001', 'Lydie', 'Lock', 'llockl1@princeton.edu', 'Female', 'Dorogobuzh', '1988-01-29', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (760, '21-0006135', '001', 'Milo', 'Wrathmall', 'mwrathmalll2@com.com', 'Male', 'San Antonio', '1983-09-13', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (761, '38-1334522', '006', 'Rowen', 'Vila', 'rvilal3@yellowbook.com', 'Male', 'Holýšov', '1991-08-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (762, '40-8539645', '005', 'Patrizius', 'Toquet', 'ptoquetl4@who.int', 'Male', 'El Chol', '1991-09-05', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (763, '23-6427905', '002', 'Zachary', 'Jedrzej', 'zjedrzejl5@godaddy.com', 'Male', 'Xichang', '1976-05-17', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (765, '62-1819539', '006', 'Cammie', 'Dominec', 'cdominecl7@apple.com', 'Female', 'Naran-mandokhu Somon', '1977-10-30', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (766, '71-3975437', '001', 'Salvatore', 'Priddy', 'spriddyl8@topsy.com', 'Male', 'Shiyuetian', '1976-01-08', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (767, '80-8602056', '006', 'Orella', 'Fores', 'oforesl9@angelfire.com', 'Female', 'Strzelce', '1988-04-03', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (768, '78-1233008', '002', 'Sib', 'Saffran', 'ssaffranla@geocities.com', 'Female', 'Tuanai', '1979-10-12', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (769, '52-5025264', '004', 'Nanete', 'Stonebridge', 'nstonebridgelb@ted.com', 'Female', 'Charlemagne', '1980-06-10', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (770, '43-5351808', '001', 'Shurlocke', 'Frounks', 'sfrounkslc@archive.org', 'Male', 'Hongde', '1978-07-30', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (771, '63-5463057', '006', 'Kristofer', 'Gadault', 'kgadaultld@google.fr', 'Male', 'Dok Kham Tai', '1977-10-12', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (772, '19-3107374', '004', 'Nathaniel', 'Koba', 'nkobale@merriam-webster.com', 'Male', 'Jetaral', '1979-10-05', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (773, '69-3643515', '004', 'Sinclare', 'Tetsall', 'stetsalllf@t-online.de', 'Male', 'Zaña', '1990-12-04', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (774, '76-5016516', '005', 'Aharon', 'Yallop', 'ayalloplg@narod.ru', 'Male', 'Marseille', '1992-02-02', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (775, '40-1211685', '005', 'Hortense', 'Busk', 'hbusklh@chronoengine.com', 'Female', 'Aygeshat', '1976-08-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (776, '72-0291574', '007', 'Anthea', 'Peggs', 'apeggsli@umich.edu', 'Female', 'Satuek', '1992-01-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (777, '47-9454112', '005', 'Bert', 'Stickney', 'bstickneylj@deviantart.com', 'Male', 'Danville', '1974-11-18', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (778, '87-2208527', '004', 'Vannie', 'Lyndon', 'vlyndonlk@mozilla.com', 'Female', 'Willemstad', '1986-08-24', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (779, '80-9134039', '007', 'Holt', 'Rapinett', 'hrapinettll@virginia.edu', 'Male', 'Kuznechnoye', '1980-10-24', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (780, '86-5301740', '004', 'Amelie', 'Mustin', 'amustinlm@surveymonkey.com', 'Female', 'Mankoeng', '1975-07-13', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (781, '27-6752170', '008', 'Jerry', 'Pringer', 'jpringerln@biglobe.ne.jp', 'Female', 'Kuźnia Raciborska', '1972-04-26', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (782, '03-1063758', '006', 'Clementine', 'Simmins', 'csimminslo@theguardian.com', 'Female', 'Itabaiana', '1984-04-25', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (783, '66-9768065', '006', 'Hetty', 'Gehrels', 'hgehrelslp@wikia.com', 'Female', 'Bayan Ewenke Minzu', '1983-08-26', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (784, '68-4715722', '007', 'Stanislaw', 'Mulryan', 'smulryanlq@tinyurl.com', 'Male', 'Gaoxiang', '1979-04-16', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (785, '73-9272868', '004', 'Meg', 'Valentinetti', 'mvalentinettilr@mediafire.com', 'Female', 'Ra’s Bayrūt', '1984-05-29', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (786, '22-4458592', '003', 'Nariko', 'Salle', 'nsallels@ameblo.jp', 'Female', 'Santa Fe', '1983-06-03', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (787, '11-7861311', '004', 'Adrianne', 'Ebbage', 'aebbagelt@vimeo.com', 'Female', 'Nizao', '1978-10-22', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (788, '02-1153877', '007', 'Sile', 'Bail', 'sbaillu@51.la', 'Female', 'Huashi', '1992-11-28', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (789, '94-5598277', '006', 'Damian', 'Hovy', 'dhovylv@bloglines.com', 'Male', 'Tateyama', '1987-04-05', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (790, '14-9540448', '008', 'Kennie', 'Hince', 'khincelw@ibm.com', 'Male', 'Shōbu', '1976-06-08', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (791, '49-9390727', '001', 'Maye', 'Bittlestone', 'mbittlestonelx@washingtonpost.com', 'Female', 'Bhokadoke', '1996-09-06', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (792, '20-1929978', '003', 'Hoebart', 'Ehrat', 'hehratly@scribd.com', 'Male', 'Petung', '1996-05-09', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (793, '02-6364402', '007', 'Carlos', 'Langhor', 'clanghorlz@w3.org', 'Male', 'Rennes', '1990-12-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (794, '96-1857462', '008', 'Kylen', 'Cordelle', 'kcordellem0@indiegogo.com', 'Female', 'Praia da Tocha', '1982-02-06', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (795, '43-5794402', '002', 'Joachim', 'Powell', 'jpowellm1@google.cn', 'Male', 'Cipeucang', '1992-12-11', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (796, '30-3719789', '008', 'Marja', 'Barrass', 'mbarrassm2@earthlink.net', 'Female', 'Encañada', '1989-08-28', 'Teknisi');
INSERT INTO "public"."ref_pegawai" VALUES (797, '52-3265961', '005', 'Elna', 'Bilbee', 'ebilbeem3@blogspot.com', 'Female', 'Xiaochuan', '1977-11-17', 'Karyawan');
INSERT INTO "public"."ref_pegawai" VALUES (798, '58-7954549', '005', 'Odele', 'Ingman', 'oingmanm4@epa.gov', 'Female', 'Kannus', '1975-03-31', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (799, '44-9861495', '001', 'Emlynn', 'Whiteford', 'ewhitefordm5@mysql.com', 'Female', 'Canta', '1993-05-04', 'Helper');
INSERT INTO "public"."ref_pegawai" VALUES (800, '04-1344915', '003', 'August', 'Sandercock', 'asandercockm6@indiatimes.com', 'Male', 'Novokuz’minki', '1980-03-04', 'Dosen');
INSERT INTO "public"."ref_pegawai" VALUES (801, '13-1616063', '004', 'Eyde', 'Cauderlie', 'ecauderliem7@google.co.jp', 'Female', 'Clermont-Ferrand', '1983-04-19', 'Karyawan');

-- ----------------------------
-- Table structure for ref_ruangan
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_ruangan";
CREATE TABLE "public"."ref_ruangan" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq6'::regclass),
  "unit_id" varchar(20) COLLATE "pg_catalog"."default",
  "ruang_id" varchar(20) COLLATE "pg_catalog"."default",
  "deskripsi" varchar(100) COLLATE "pg_catalog"."default",
  "rent" int2,
  "biaya" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_ruangan
-- ----------------------------
INSERT INTO "public"."ref_ruangan" VALUES (1, '002', '001', 'Ruang Seminar 2', 0, NULL);
INSERT INTO "public"."ref_ruangan" VALUES (2, '002', '002', 'Ruang Seminar 3', 0, NULL);
INSERT INTO "public"."ref_ruangan" VALUES (3, '002', '003', 'Ruang Dekan', 0, NULL);
INSERT INTO "public"."ref_ruangan" VALUES (4, '002', '004', 'Lab SCCIC', 0, NULL);
INSERT INTO "public"."ref_ruangan" VALUES (5, '000', '005', 'GKU Timur', 1, '500000');
INSERT INTO "public"."ref_ruangan" VALUES (6, '000', '006', 'GKU Barat', 1, '450000');

-- ----------------------------
-- Table structure for ref_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_status";
CREATE TABLE "public"."ref_status" (
  "id" int4 NOT NULL,
  "status_id" int4,
  "status" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_status
-- ----------------------------
INSERT INTO "public"."ref_status" VALUES (1, 1, 'Belum Terverifikasi');
INSERT INTO "public"."ref_status" VALUES (2, 2, 'Sudah Terverifikasi');

-- ----------------------------
-- Table structure for ref_status_ajuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_status_ajuan";
CREATE TABLE "public"."ref_status_ajuan" (
  "code" int4 NOT NULL,
  "deskripsi" varchar(30) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of ref_status_ajuan
-- ----------------------------
INSERT INTO "public"."ref_status_ajuan" VALUES (0, 'Belum diperiksa');
INSERT INTO "public"."ref_status_ajuan" VALUES (1, 'Proses verifikasi');
INSERT INTO "public"."ref_status_ajuan" VALUES (5, 'Ditolak');
INSERT INTO "public"."ref_status_ajuan" VALUES (2, 'Menunggu pembayaran');
INSERT INTO "public"."ref_status_ajuan" VALUES (4, 'Disetujui');
INSERT INTO "public"."ref_status_ajuan" VALUES (3, 'Dibayar');

-- ----------------------------
-- Table structure for ref_unit
-- ----------------------------
DROP TABLE IF EXISTS "public"."ref_unit";
CREATE TABLE "public"."ref_unit" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq7'::regclass),
  "unit_id" varchar(20) COLLATE "pg_catalog"."default",
  "name" varchar(200) COLLATE "pg_catalog"."default",
  "activated" int4,
  "pejabat" int2
)
;

-- ----------------------------
-- Records of ref_unit
-- ----------------------------
INSERT INTO "public"."ref_unit" VALUES (1, '001', 'Rektorat', 1, 12);
INSERT INTO "public"."ref_unit" VALUES (2, '002', 'STEI', 1, 18);
INSERT INTO "public"."ref_unit" VALUES (3, '003', 'FSRD', 1, 19);
INSERT INTO "public"."ref_unit" VALUES (4, '004', 'UPTK3L', 1, 13);
INSERT INTO "public"."ref_unit" VALUES (5, '005', 'DISTI', 1, 12);
INSERT INTO "public"."ref_unit" VALUES (6, '006', 'Dir. Keuangan', 1, 14);
INSERT INTO "public"."ref_unit" VALUES (7, '007', 'Dir. Logistik', 1, 16);
INSERT INTO "public"."ref_unit" VALUES (8, '008', 'Dir. Sarpras', 1, 16);

-- ----------------------------
-- Table structure for temp_trans_kebutuhan
-- ----------------------------
DROP TABLE IF EXISTS "public"."temp_trans_kebutuhan";
CREATE TABLE "public"."temp_trans_kebutuhan" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq8'::regclass),
  "kebutuhan_id" varchar(20) COLLATE "pg_catalog"."default",
  "asset_id" varchar(20) COLLATE "pg_catalog"."default",
  "jumlah" int2,
  "activated" int2 DEFAULT 1,
  "keterangan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of temp_trans_kebutuhan
-- ----------------------------
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (5, '5cb6b3470dbbd', '12347', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (6, '30fb0e42', '12348', 1, 1, 'ok');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (7, '4d1b8ef2', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (8, 'f70afe70', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (9, 'cb748154', '12351', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (10, '8cba6d78', '12351', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (11, '8cba6d78', '12347', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (12, 'fc20af23', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (13, 'cb48b516', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (14, 'cb48b516', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (15, '5bc09163', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (16, '5bc09163', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (17, 'e9fd8313', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (18, 'fe09eb8b', '12348', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (19, '23d6a935', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (20, '07da0c50', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (21, 'e4d1259e', '12351', 1, 1, 'd');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (22, 'e4d1259e', '12365', 1, 1, 'kkk');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (23, 'f9a34b86', '12351', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (24, '06169a9a', '12351', 3, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (33, 'bcf70ad3', '12351', 3, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (35, 'bcf70ad3', '12346', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (37, '2d5cfeca', '12351', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (38, '9f0c1e07', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (40, 'c2b5c8d1', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (41, 'c2b5c8d1', '12348', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (42, 'cc129647', '12351', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (43, 'cc129647', '12361', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (44, 'e7bbdafe', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (45, 'e7bbdafe', '12360', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (46, 'a168adbe', '12356', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (47, 'a168adbe', '12354', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (48, '14c2533e', '12356', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (49, '14c2533e', '12352', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (50, '14c2533e', '12347', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (51, 'ce08cea1', '12345', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (52, 'ce08cea1', '12363', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (54, '2e6ea968', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (55, '50f40b25', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (56, '50f40b25', '12351', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (57, '25f3ba89', '12351', 1, 2, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (59, '25f3ba89', '12347', 2, 2, '2');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (58, '25f3ba89', '12346', 1, 0, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (60, '25f3ba89', '12349', 1, 0, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (61, 'eaa7e102', '12351', 1, 1, '1');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (62, '2d1a8b38', '12349', 1, 0, '@');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (64, 'cfc3442e', '12356', 1, 0, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (63, '2d1a8b38', '12347', 2, 2, '@');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (67, 'a1b41d79', '12351', 1, 2, '9');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (66, '8649a606', '12346', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (65, '8649a606', '12348', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (68, '560542946', '12358', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (69, '560542946', '12355', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (70, '5cb6b3470dbbd', '12347', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (72, '9cfe0df0', '12350', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (71, '9cfe0df0', '12350', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (73, '9cfe0df0', '12363', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (75, 'e2cafdfa', '12358', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (74, 'e2cafdfa', '12358', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (77, 'e2cafdfa', '12345', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (76, 'e2cafdfa', '12345', 1, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (78, 'c186df10', '12363', 2, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (79, 'fd152c29', '12351', 1, 1, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (80, 'be76f541', '12351', 1, 0, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (81, '430cfa64', '12354', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (82, '430cfa64', '12356', 2, 2, '-');
INSERT INTO "public"."temp_trans_kebutuhan" VALUES (83, '430cfa64', '12364', 2, 2, '-');

-- ----------------------------
-- Table structure for trans_kebutuhan
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_kebutuhan";
CREATE TABLE "public"."trans_kebutuhan" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq'::regclass),
  "kebutuhan_id" varchar(20) COLLATE "pg_catalog"."default",
  "unit_id" varchar(20) COLLATE "pg_catalog"."default",
  "asset_id" varchar(20) COLLATE "pg_catalog"."default",
  "jumlah" int4,
  "keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "status_id" int4,
  "unit_id_verifikasi" varchar(20) COLLATE "pg_catalog"."default",
  "date" timestamp(6)
)
;

-- ----------------------------
-- Records of trans_kebutuhan
-- ----------------------------
INSERT INTO "public"."trans_kebutuhan" VALUES (8, '5cb6b0b938042', '001', '12350', 4, 'tes', 1, NULL, '2019-04-17 11:04:05');
INSERT INTO "public"."trans_kebutuhan" VALUES (10, '5cb6b3470dbbd', '001', '12350', 4, 'tes', 1, NULL, '2019-04-17 12:04:59');
INSERT INTO "public"."trans_kebutuhan" VALUES (11, '5cb6bd9025a33', '004', '12350', 2, 'tes', 1, NULL, '2019-04-17 12:04:52');
INSERT INTO "public"."trans_kebutuhan" VALUES (13, '5cb6be183d224', '004', '12350', 2, 'tes', 1, NULL, '2019-04-17 12:04:08');
INSERT INTO "public"."trans_kebutuhan" VALUES (12, '5cb6bdcf7f15b', '004', '12354', 2, 'tes', 1, NULL, '2019-04-17 12:04:55');
INSERT INTO "public"."trans_kebutuhan" VALUES (14, '5cb6be7d44ca0', '004', '12354', 2, 'tes', 1, NULL, '2019-04-17 12:04:49');
INSERT INTO "public"."trans_kebutuhan" VALUES (9, '5cb6b0cc8b1fa', '001', '12350', 4, 'tes', 1, NULL, '2019-04-17 11:04:24');
INSERT INTO "public"."trans_kebutuhan" VALUES (19, '5cb6bd9025a33', '004', '12350', 1, 'disetujui', 1, '006', '2019-04-17 01:04:41');
INSERT INTO "public"."trans_kebutuhan" VALUES (18, '5cb6be7d44ca0', '004', '12356', 1, 'disetujui', 1, '006', '2019-04-17 01:04:07');
INSERT INTO "public"."trans_kebutuhan" VALUES (40, 'a1b41d79', '008', '12351', 1, '9', 1, NULL, '2019-04-18 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (39, '8649a606', '008', '12346', 1, '-', 1, NULL, '2019-04-18 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (38, '8649a606', '008', '12348', 1, '-', 1, NULL, '2019-04-18 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (37, 'cfc3442e', '008', '12356', 1, '-', 1, NULL, '2019-04-18 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (21, '5cb7dad2b7157', '008', '12347', 2, '-', 1, NULL, '2019-04-18 09:04:58');
INSERT INTO "public"."trans_kebutuhan" VALUES (22, '5cb7db27d9947', '008', '12347', 2, '-', 1, NULL, '2019-04-18 09:04:23');
INSERT INTO "public"."trans_kebutuhan" VALUES (16, '5cb6b3470dbbd', '002', '12356', 2, 'disetujui', 1, '006', '2019-04-05 15:05:20');
INSERT INTO "public"."trans_kebutuhan" VALUES (20, '5cb6b0cc8b1fa', '001', '12350', 11, '-', 1, '006', '2019-04-17 10:04:58');
INSERT INTO "public"."trans_kebutuhan" VALUES (1, '5cb6b3470dbbd', '002', '12346', 3, '-', 2, '002', '2019-04-05 14:52:41');
INSERT INTO "public"."trans_kebutuhan" VALUES (5, '5cb6b3470dbbd', '002', '12346', 1, '-', 1, '002', '2019-04-05 15:05:20');
INSERT INTO "public"."trans_kebutuhan" VALUES (41, '560542946', '008', '12358', 1, '-', 1, NULL, '2019-04-19 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (42, '560542946', '008', '12355', 2, '-', 1, NULL, '2019-04-19 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (43, '5cbab8dbf1cde', '002', '12350', 2, 'tes', 1, NULL, '2019-04-20 01:04:51');
INSERT INTO "public"."trans_kebutuhan" VALUES (44, '5cbab938874d3', '002', '12350', 2, 'tes', 1, NULL, '2019-04-20 01:04:24');
INSERT INTO "public"."trans_kebutuhan" VALUES (45, '5cb6b3470dbbd', '001', '12350', 11, '-', NULL, '002', '2019-04-20 01:04:54');
INSERT INTO "public"."trans_kebutuhan" VALUES (46, '9cfe0df0', '002', '12350', 2, '-', 1, NULL, '2019-04-20 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (47, '9cfe0df0', '002', '12363', 1, '-', 1, NULL, '2019-04-20 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (48, 'e2cafdfa', '002', '12358', 1, '-', 1, NULL, '2019-04-22 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (49, 'e2cafdfa', '002', '12345', 1, '-', 1, NULL, '2019-04-22 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (50, '430cfa64', '002', '12354', 2, '-', 1, NULL, '2019-05-13 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (51, '430cfa64', '002', '12356', 2, '-', 1, NULL, '2019-05-13 00:00:00');
INSERT INTO "public"."trans_kebutuhan" VALUES (52, '430cfa64', '002', '12364', 2, '-', 1, NULL, '2019-05-13 00:00:00');

-- ----------------------------
-- Table structure for trans_keluhan
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_keluhan";
CREATE TABLE "public"."trans_keluhan" (
  "id" int4 NOT NULL DEFAULT nextval('sequence_keluhan'::regclass),
  "unit_id" varchar COLLATE "pg_catalog"."default",
  "user_id" varchar COLLATE "pg_catalog"."default",
  "judul" varchar(32) COLLATE "pg_catalog"."default",
  "date" date,
  "deskripsi" text COLLATE "pg_catalog"."default",
  "status" int2 DEFAULT nextval('sequence_perizinan'::regclass),
  "solution" text COLLATE "pg_catalog"."default",
  "assign_staff" varchar(30) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of trans_keluhan
-- ----------------------------
INSERT INTO "public"."trans_keluhan" VALUES (3, '002', '40-8441155', 'Pipa air bocor', '2019-05-01', 'Seminar XYZ', 0, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (4, '002', '40-8441155', 'Air macet', '2019-05-12', NULL, 5, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (5, '002', '40-8441155', 'Air macet', '2019-05-12', '', 5, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (6, '002', '40-8441155', 'Air macet', '2019-05-12', '', 5, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (2, '002', '40-8441155', 'Tembok retak', '2019-05-11', 'Seminar Smart City', 0, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (7, '002', '40-8441155', 'Air macet', '2019-05-12', 'Macet-macet', 0, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (1, '002', '40-8441155', 'Atap bocor', '2019-05-06', 'Bocor atap', 0, 'Menambal atap', '58-8145670');
INSERT INTO "public"."trans_keluhan" VALUES (8, '002', '40-8441155', 'Pohon tumbang', '2019-05-13', 'bla bla', 0, NULL, NULL);
INSERT INTO "public"."trans_keluhan" VALUES (9, '002', '40-8441155', 'kaca pecah', '2019-05-13', 'kaca pecah di aula timur gku', 0, NULL, NULL);

-- ----------------------------
-- Table structure for trans_pengadaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_pengadaan";
CREATE TABLE "public"."trans_pengadaan" (
  "id" int4 NOT NULL DEFAULT nextval('trans_pengadaan_id'::regclass),
  "kebutuhan_id" varchar(20) COLLATE "pg_catalog"."default",
  "jenis_pengadaan" int2,
  "hps" float8,
  "status" int2
)
;

-- ----------------------------
-- Records of trans_pengadaan
-- ----------------------------
INSERT INTO "public"."trans_pengadaan" VALUES (1, '8649a606', 1, 65000000, 1);
INSERT INTO "public"."trans_pengadaan" VALUES (2, '5cb7dad2b7157', 2, 210000000, 2);

-- ----------------------------
-- Table structure for trans_perawatan
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_perawatan";
CREATE TABLE "public"."trans_perawatan" (
  "id" int4 NOT NULL DEFAULT nextval('sequence_perawatan'::regclass),
  "schedule" date,
  "asset_condition" text COLLATE "pg_catalog"."default",
  "maintenance" varchar(255) COLLATE "pg_catalog"."default",
  "assign_staff" varchar(30) COLLATE "pg_catalog"."default",
  "solution" text COLLATE "pg_catalog"."default",
  "status" int2,
  "oleh" varchar(30) COLLATE "pg_catalog"."default",
  "asset" varchar(255) COLLATE "pg_catalog"."default",
  "unit_id" varchar(32) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of trans_perawatan
-- ----------------------------
INSERT INTO "public"."trans_perawatan" VALUES (1, '2019-05-14', 'Pengecatan Labtek IX', 'Atap bocor pada Labtek IX', '07-9513502', 'Tambal atap', 0, '99-6132128', NULL, '002');
INSERT INTO "public"."trans_perawatan" VALUES (2, '2019-05-23', 'Pemangkasan pohon', 'Tolong ada apa pada Labtek IX', '07-9513502', 'Tambal atap', 3, '99-6132128', NULL, '002');
INSERT INTO "public"."trans_perawatan" VALUES (3, '2019-05-22', 'Pengaspalan pada area parkir', 'Awas ada lubang pada Labtek IX', '07-9513502', 'Tambal atap', 2, '99-6132128', NULL, '002');
INSERT INTO "public"."trans_perawatan" VALUES (6, '2019-05-02', 'Perawatan tiang', 'heelp', NULL, NULL, 0, '99-6132128', NULL, '002');
INSERT INTO "public"."trans_perawatan" VALUES (7, '2019-05-20', 'pengecetan labtek 8', 'Pengecetan lantai 1', NULL, NULL, 0, '99-6132128', NULL, '002');
INSERT INTO "public"."trans_perawatan" VALUES (8, NULL, NULL, NULL, NULL, NULL, 0, '99-6132128', NULL, NULL);

-- ----------------------------
-- Table structure for trans_perizinan
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_perizinan";
CREATE TABLE "public"."trans_perizinan" (
  "id" int4 NOT NULL DEFAULT nextval('sequence_perizinan'::regclass),
  "unit_id" varchar COLLATE "pg_catalog"."default",
  "user_id" varchar COLLATE "pg_catalog"."default",
  "ruang_id" varchar(32) COLLATE "pg_catalog"."default",
  "date_in" date,
  "date_out" date,
  "deskripsi" varchar(255) COLLATE "pg_catalog"."default",
  "status" int2 DEFAULT nextval('sequence_perizinan'::regclass),
  "assign_staff" varchar(32) COLLATE "pg_catalog"."default",
  "oleh" varchar(32) COLLATE "pg_catalog"."default",
  "date" date
)
;

-- ----------------------------
-- Records of trans_perizinan
-- ----------------------------
INSERT INTO "public"."trans_perizinan" VALUES (7, '002', '40-8441155', '006', '2019-05-01', '2019-05-04', 'Seminar XYZ', 0, NULL, NULL, NULL);
INSERT INTO "public"."trans_perizinan" VALUES (2, '002', '40-8441155', '005', '2019-05-11', '2019-05-17', 'Seminar Smart City', 5, NULL, NULL, NULL);
INSERT INTO "public"."trans_perizinan" VALUES (8, '002', '40-8441155', '005', '2019-05-14', '2019-05-16', 'SEMINAR', 5, NULL, NULL, NULL);
INSERT INTO "public"."trans_perizinan" VALUES (1, '002', '40-8441155', '005', '2019-05-06', '2019-05-08', 'Pameran Teknologi', 5, '75-3659793', '40-8441155', '2019-05-12');
INSERT INTO "public"."trans_perizinan" VALUES (9, '002', '40-8441155', '006', '2019-05-08', '2019-05-23', '', 5, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for trans_stock
-- ----------------------------
DROP TABLE IF EXISTS "public"."trans_stock";
CREATE TABLE "public"."trans_stock" (
  "id" int4 NOT NULL DEFAULT nextval('id_seq2'::regclass),
  "sn" varchar(40) COLLATE "pg_catalog"."default",
  "unit_id" varchar(20) COLLATE "pg_catalog"."default",
  "ruang_id" varchar(20) COLLATE "pg_catalog"."default",
  "asset_id" varchar(20) COLLATE "pg_catalog"."default",
  "kondisi_id" int2,
  "date" timestamp(6),
  "activated" int4
)
;

-- ----------------------------
-- Records of trans_stock
-- ----------------------------
INSERT INTO "public"."trans_stock" VALUES (20, '4010009', '008', '004', '12358', 1, '2019-04-01 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (18, '4010007', '008', '004', '12356', 1, '2019-04-01 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (17, '4010006', '008', '004', '12355', 1, '2019-04-01 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (15, '4010004', '008', '004', '12353', 1, '2019-04-02 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (8, '4009997', '002', '004', '12348', 1, '2019-04-03 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (14, '4010003', '004', '001', '12361', 1, '2019-04-02 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (2, '4009991', '001', '003', '12345', 1, '2019-04-05 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (3, '4009992', '001', '001', '12346', 1, '2019-04-05 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (4, '4009993', '001', '002', '12347', 1, '2019-04-05 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (11, '4010000', '004', '004', '12357', 1, '2019-04-02 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (13, '4010002', '004', '001', '12364', 1, '2019-04-02 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (19, '4010008', '002', '004', '12357', 2, '2019-04-01 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (16, '4010005', '002', '004', '12354', 1, '2019-04-01 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (10, '4009999', '002', '003', '12345', 1, '2019-04-03 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (12, '4010001', '002', '001', '12349', 1, '2019-04-02 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (6, '4009995', '002', '001', '12354', 2, '2019-04-05 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (5, '4009994', '002', '001', '12361', 2, '2019-04-05 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (9, '4009998', '002', '002', '12357', 2, '2019-04-03 00:00:00', 1);
INSERT INTO "public"."trans_stock" VALUES (7, '4009996', '002', '002', '12352', 2, '2019-04-03 00:00:00', 1);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."id_seq"', 53, true);
SELECT setval('"public"."id_seq1"', 22, true);
SELECT setval('"public"."id_seq2"', 2, false);
SELECT setval('"public"."id_seq3"', 24, true);
SELECT setval('"public"."id_seq4"', 5, true);
SELECT setval('"public"."id_seq5"', 6, true);
SELECT setval('"public"."id_seq6"', 7, true);
SELECT setval('"public"."id_seq7"', 10, true);
SELECT setval('"public"."id_seq8"', 84, true);
SELECT setval('"public"."sequence_keluhan"', 10, true);
SELECT setval('"public"."sequence_perawatan"', 9, true);
SELECT setval('"public"."sequence_perizinan"', 10, true);
SELECT setval('"public"."trans_pengadaan_id"', 3, true);
SELECT setval('"public"."user_id_plus1"', 803, true);

-- ----------------------------
-- Primary Key structure for table ref_barang
-- ----------------------------
ALTER TABLE "public"."ref_barang" ADD CONSTRAINT "ref_barang_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ref_kondisi
-- ----------------------------
ALTER TABLE "public"."ref_kondisi" ADD CONSTRAINT "ref_kondisi_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ref_pegawai
-- ----------------------------
ALTER TABLE "public"."ref_pegawai" ADD CONSTRAINT "ref_pegawai_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ref_ruangan
-- ----------------------------
ALTER TABLE "public"."ref_ruangan" ADD CONSTRAINT "ref_ruangan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ref_status
-- ----------------------------
ALTER TABLE "public"."ref_status" ADD CONSTRAINT "ref_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ref_status_ajuan
-- ----------------------------
ALTER TABLE "public"."ref_status_ajuan" ADD CONSTRAINT "ref_status_perizinan_pkey" PRIMARY KEY ("code");

-- ----------------------------
-- Primary Key structure for table ref_unit
-- ----------------------------
ALTER TABLE "public"."ref_unit" ADD CONSTRAINT "ref_unit_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table temp_trans_kebutuhan
-- ----------------------------
ALTER TABLE "public"."temp_trans_kebutuhan" ADD CONSTRAINT "temp_trans_kebutuhan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_kebutuhan
-- ----------------------------
ALTER TABLE "public"."trans_kebutuhan" ADD CONSTRAINT "trans_kebutuhan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_keluhan
-- ----------------------------
ALTER TABLE "public"."trans_keluhan" ADD CONSTRAINT "trans_perizinan_copy1_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_pengadaan
-- ----------------------------
ALTER TABLE "public"."trans_pengadaan" ADD CONSTRAINT "trans_pengadaan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_perawatan
-- ----------------------------
ALTER TABLE "public"."trans_perawatan" ADD CONSTRAINT "trans_perawatan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_perizinan
-- ----------------------------
ALTER TABLE "public"."trans_perizinan" ADD CONSTRAINT "trans_perizinan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trans_stock
-- ----------------------------
ALTER TABLE "public"."trans_stock" ADD CONSTRAINT "trans_stock_pkey" PRIMARY KEY ("id");
